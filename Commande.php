<!DOCTYPE html>
<!--[if lt IE 10]>
<html lang="en" class="iex"> <![endif]-->
<!--[if (gt IE 10)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from templates.framework-y.com/gourmet/pages/features/components/php-contact-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 Jan 2018 01:57:54 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Commande | Royal | Pizzeria | Kebab </title>
    <meta name="description" content="Multipurpose HTML template.">
    <script src="http://templates.framework-y.com/gourmet/scripts/jquery.min.js"></script>
    <link rel="stylesheet" href="http://templates.framework-y.com/gourmet/scripts/bootstrap/css/bootstrap.css">
    <script src="assets/js/script.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/content-box.css">
    <link rel="stylesheet" href="assets/css/image-box.css">
    <link rel="stylesheet" href="assets/css/animations.css">
    <link rel="stylesheet" href='assets/css/components.css'>
    <link rel="stylesheet" href='assets/css/flexslider.css'>
    <link rel="stylesheet" href='assets/css/magnific-popup.css'>
    <link rel="stylesheet" href='assets/css/contact-form.css'>
    <link rel="stylesheet" href='assets/css/social.stream.css'>
    <link rel="icon" href='assets/images/pi.ico'>
    <link rel="stylesheet" href="assets/css/skin.css">
    <link rel="stylesheet" href="assets/css/Cardstyle.css">
    <!-- Extra optional content header -->
</head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){
    background-color: #FAFAFA;
    opacity: 0.7;
    }
</style>
<body>
<div id="preloader"></div>
<header class="fixed-top bg-transparent menu-transparent scroll-change wide-area" data-menu-anima="fade-in">
    <div class="navbar navbar-default mega-menu-fullwidth navbar-fixed-top" role="navigation">
        <div class="navbar navbar-main">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="index.php">
                        <img class="logo-default scroll-hide" src="assets/images/RFB2.png" alt="logo" />
                        <img class="logo-default scroll-show" src="assets/images/RFN2.png" alt="logo" />
                        <img class="logo-retina" src="http://templates.framework-y.com/gourmet/images/logo-retina.png" alt="logo" />
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown active">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Acceuil <span class="caret"></span></a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="Commande.php">Commandez <span class="caret"></span></a>

                        </li>
                        <li class="dropdown">
                            <a href="contactUs.php" class="dropdown-toggle" data-toggle="dropdown" role="button">Nous Contacter <span class="caret"></span></a>

                        </li>

                    </ul>

                </div>
            </div>
        </div>
    </div>
</header>

<div class="header-title white ken-burn" data-parallax="scroll" data-position="top" style="opacity: 0.6;"
     data-natural-height="850" data-natural-width="1920" data-image-src="assets/images/pizzabackground1.jpg">
    <div class="container">
        <div class="title-base">
            <h1>Commandez votre Pizza</h1>

        </div>
    </div>
</div>
<div class="section-item text-center">

    <div class="container-fluid" id="command">

        <div class="row" id="command">

            <div class="col-md-2"  >
                <div class="card" style="background-image: url(assets/images/MB.jpg);">

                    <div class="row" align="left" id="menu">
                        <table>
                        <tr>
                            <th><font color='#0404B4'>
                            Ville</font>
                            </th>
                            <th><font color='0404B4'>
                            Minimum commande</font>
                            </th>
                        </tr>
                        <tr>
                            <td>
                            Vevey
                            </td>
                            <td>
                            26 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Attalens
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Bossonnens
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Châtel-ST- Denis
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Les Pacots
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Aigle
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Blonay
                            </td>
                            <td>
                            30 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Chailly-Montreux
                            </td>
                            <td>
                            50 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Chardonne
                            </td>
                            <td>
                            35 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Clarens
                            </td>
                            <td>
                            30 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Corseaux
                            </td>
                            <td>
                            26 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Corsier-Sur- Vevey
                            </td>
                            <td>
                            26 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            La Conversion
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            La Croix (Lutry)
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            La Tour-De- Peilz
                            </td>
                            <td>
                            26 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Le Mont Pèlerin
                            </td>
                            <td>
                            50 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Les Monts-De- Corsier
                            </td>
                            <td>
                            35 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Lutry
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Montreux
                            </td>
                            <td>
                            35 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Noville
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Paudex
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Puidoux
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Pully
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Rennaz
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Rivaz
                            </td>
                            <td>
                            30 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Roche VD
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            St-Légier
                            </td>
                            <td>
                            26 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Territet
                            </td>
                            <td>
                            35 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Villeneuve VD
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Yvorne
                            </td>
                            <td>
                            60 CHF
                            </td>
                        </tr>
                        
                        </table>

                    </div>

                </div>


        </div>
        <div id="products" class="col-md-6" style="height: 1260px;" data-spy="scroll" data-target="#menu" data-offset="50">
            <div class="col-md-12"  style="margin-top: 1cm" id="cid-Pizza">
                <h2>Nos Pizza</h2>
                <hr>
            </div>
            <div style="margin-top: 1cm;" class="col-md-12"  id="cid-Tacos" >
                <h2>Tacos</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/tacos.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Tacos</h4>
                        <p>choisissez votre tacos et selectionnez les ingrédients</p>

                    </div>
                    <div class="container" id="menusandwichkebab">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totaltacos">Total : 10.00 CHF</p>
                                <p class="col-md-12" id="totall2tacos" style="visibility: hidden">10.00</p>


                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="typetacos">
                                    <option class="text-center" value="normal">normal</option>
                                    <option class="text-center" value="XXL">XXL</option>
                                    <option class="text-center" value="Gigatacos">Gigatacos</option>


                                </select>

                            </div>
                            <div class="row" style="margin-top: 1cm;" id="garniturestacoss">
                                <h3>-liste viandes :</h3><br>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="ViandesHaches" value="ViandesHaches" class="garniturestacos">
                                    <label>Viandes Hachés  0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Nuggets" value="Nuggets" class="garniturestacos">
                                    <label>Nuggets  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Cordonbleu" value="Cordonbleu" class="garniturestacos">
                                    <label>Cordon bleu  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Emincedepoulet" value="Emincedepoulet" class="garniturestacos">
                                    <label>Emincé de poulet  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Merguez" value="Merguez" class="garniturestacos">
                                    <label>Merguez 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Falafel" value="Falafel" class="garniturestacos">
                                    <label>Falafel 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="PouletKabeb" value="PouletKabeb" class="garniturestacos">
                                    <label>Poulet Kabeb 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="AgneauKebab" value="AgneauKebab" class="garniturestacos">
                                    <label>Agneau Kebab  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="thon" value="thon" class="garniturestacos">
                                    <label>thon  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Calamar" value="Calamar" class="garniturestacos">
                                    <label> Calamar  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Merlanpoissonpannee" value="Merlanpoissonpannee" class="garniturestacos">
                                    <label>Merlan (poisson pannée) 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Vegetarienlegumesgrille" value="Vegetarienlegumesgrille" class="garniturestacos">
                                    <label>Végétarien (légumes grillé) 0.00</label>
                                </div>
                                <h3>-liste des sauces :</h3><br>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Algerienne" value="Algerienne" class="garniturestacos">
                                    <label>Algérienne 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Andalouse" value="Andalouse" class="garniturestacos">
                                    <label> 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Ketchup" value="Ketchup" class="garniturestacos">
                                    <label>Ketchup  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Lail" value="Lail" class="garniturestacos">
                                    <label>L’ail 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Marocaine" value="Marocaine" class="garniturestacos">
                                    <label>Marocaine 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Samourai" value="Samourai" class="garniturestacos">
                                    <label>Samouraï 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Mayonnaise" value="Mayonnaise" class="garniturestacos">
                                    <label>Mayonnaise 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="BiggiBurger" value="BiggiBurger" class="garniturestacos">
                                    <label>Biggi-Burger  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="barbecue" value="barbecue" class="garniturestacos">
                                    <label>barbecue  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Cocktail" value="Cocktail" class="garniturestacos">
                                    <label>Cocktail 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Brasil" value="Brasil" class="garniturestacos">
                                    <label>Brasil 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Curry" value="Curry" class="garniturestacos">
                                    <label>Curry 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Tartare" value="Tartare" class="garniturestacos">
                                    <label>Tartare 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Blanche" value="Blanche" class="garniturestacos">
                                    <label>Blanche 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Americaine" value="Americaine" class="garniturestacos">
                                    <label>Américaine 0.00</label>
                                </div>
                                <h3>-Nos fromages :</h3><br>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="SauceFromagere" value="SauceFromagere" class="garniturestacos">
                                    <label>Sauce Fromagère  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Gruyererape" value="Gruyererape" class="garniturestacos">
                                    <label>Gruyère râpé 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Cheddar" value="Cheddar" class="garniturestacos">
                                    <label>Cheddar 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Mozzarella" value="Mozzarella" class="garniturestacos">
                                    <label>Mozzarella 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Raclette" value="Raclette" class="garniturestacos">
                                    <label>Raclette 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Vachequirit" value="Vachequirit" class="garniturestacos">
                                    <label>Vache qui-rit 0.00</label>
                                </div>
                                <h3>-Nos garnitures :</h3><br>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Frites" value="Frites" class="garniturestacos">
                                    <label>Frites 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Salade" value="Salade" class="garniturestacos">
                                    <label>Salade 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Carotte" value="Carotte" class="garniturestacos">
                                    <label>Carotte 0.00</label>
                                </div>



                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Oignon" value="Oignon" class="garniturestacos">
                                    <label>Oignon 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Tomate" value="Tomate" class="garniturestacos">
                                    <label>Tomate 0.00</label>
                                </div>






                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandeztacos"> Commandez</button>
                            </div>
                        </div>

                    </div></div></div>
            <div style="margin-top: 1cm;" class="col-md-12" id="cid-Kebab">
                <h2>Kebab & Sandwich</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/kabebetsandwitch.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Kebab & Sandwich</h4>
                        <p>choisissez votre sandwich ou votre kebab  et selectionnez les ingrédients</p>

                    </div>
                    <div class="container" id="menusandwichkebab">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalsandwichkabeb">Total : 12.00 CHF</p>
                                <p class="col-md-12" id="totall2sandwichkabeb" style="visibility: hidden">12.00</p>


                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="sandwichkebab">
                                    <option class="text-center" value="DürümKebabagneau">Dürüm Kebab agneau</option>
                                    <option class="text-center" value="DürümKebabpoulet">Dürüm Kebab poulet</option>
                                    <option class="text-center" value="DürümKebabagneauetpouletmixte">Dürüm Kebab agneau et poulet mixte</option>
                                    <option class="text-center" value="DürümKöfte">Dürüm Köfte</option>
                                    <option class="text-center" value="DürümFalafel">Dürüm Falafel</option>
                                    <option class="text-center" value="SandwichKebabagneau">Sandwich Kebab agneau</option>
                                    <option class="text-center" value="SandwichKebabpoulet">Sandwich Kebab poulet</option>
                                    <option class="text-center" value="SandwichKebabagneauetpouletmixte">Sandwich Kebab agneau et poulet mixte</option>
                                    <option class="text-center" value="Sandwichgigotdagneau">Sandwich gigot d'agneau</option>
                                    <option class="text-center" value="Sandwichpouletgrillé">Sandwich poulet grillé</option>
                                    <option class="text-center" value="Chickennuggetsbox">Chicken nuggets box</option>
                                    <option class="text-center" value="Calamarsbox"> Calamars box</option>

                                </select>

                            </div>
                            <div class="row" style="margin-top: 1cm;" id="garnituresandwichbabeb">

                                <div class="col-md-6">
                                    <input type="checkbox" id="salade" value="salade" class="garnituresandwichbabeb">
                                    <label>salade 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="tomates" value="tomates" class="garnituresandwichbabeb">
                                    <label>tomates 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="chourouge" value="chourouge" class="garnituresandwichbabeb">
                                    <label>Chou rouge 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="oignons" value="oignons" class="garnituresandwichbabeb">
                                    <label>Oignons 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="carottes" value="carottes" class="garnituresandwichbabeb">
                                    <label>Carottes 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="sauceblanche" value="sauceblanche" class="garnituresandwichbabeb">
                                    <label>sauce blanche 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucealgérienne" value="saucealgérienne" class="garnituresandwichbabeb">
                                    <label>sauce algérienne 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucesamouraï" value="saucesamouraï" class="garnituresandwichbabeb">
                                    <label>sauce samouraï 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucetartare" value="saucetartare" class="garnituresandwichbabeb">
                                    <label>sauce tartare 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucecocktail" value="saucecocktail" class="garnituresandwichbabeb">
                                    <label>sauce cocktail 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucemarocaine" value="saucemarocaine" class="garnituresandwichbabeb">
                                    <label>sauce marocaine 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucecurry" value="saucecurry" class="garnituresandwichbabeb">
                                    <label>sauce curry 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucealail" value="saucealail" class="garnituresandwichbabeb">
                                    <label>sauce à l'ail 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="mayonnaise" value="mayonnaise" class="garnituresandwichbabeb">
                                    <label>mayonnaise 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="ketchup" value="ketchup" class="garnituresandwichbabeb">
                                    <label>ketchup 0.00</label>
                                </div>




                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezsandwichkabeb"> Commandez</button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/pizzabackground1.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Menu pizza</h4>
                        <p>Obtenir votre pizza</p>

                    </div>
                    <div class="container" id="menuchoicepizza">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="col-md-6"><p>Pizza </p></div>
                            <div class="col-md-6 " style="">

                                <select class="form-control" id="pizzachoice" onchange="loadpizza()">
                                    <option class="text-center"  value="merguez">Pizza merguez</option>
                                    <option class="text-center" value="Pizza margerite">Pizza margerite</option>
                                    <option class="text-center" value="Pizza Thon">Pizza Thon</option>
                                    <option class="text-center" value="Pizza Thai">Pizza Thai</option>
                                    <option class="text-center" value="Pizza Romana">Pizza Romana</option>
                                    <option class="text-center" value="Pizza 4 Saisons">Pizza 4 saisons</option>
                                    <option class="text-center" value="Pizza Crevettes">Pizza Crevettes</option>
                                    <option class="text-center" value="Pizza Fruits De Mer">Pizza Fruits De Mer</option>
                                    <option class="text-center" value="Pizza Sucuk">Pizza Sucuk (saucisse turc)</option>
                                    <option class="text-center" value="Pizza Royal">Pizza Royal</option>
                                    <option class="text-center" value="Pizza Jambon">Pizza Jambon</option>
                                    <option class="text-center" value="Pizza Diavola">Pizza Diavola</option>
                                    <option class="text-center" value="Pizza Hawai">Pizza Hawai</option>
                                    <option class="text-center" value="Pizza Napoli">Pizza Napoli</option>
                                    <option class="text-center" value="Pizza Vegetarienne">Pizza Végétarienne</option>
                                    <option class="text-center" value="Pizza Tunisienne">Pizza Tunisienne</option>
                                    <option class="text-center" value="Pizza Quatre Fromages">Pizza Quatre Fromages</option>
                                    <option class="text-center" value="Pizza Calzona">Pizza Calzona</option>
                                    <option class="text-center" value="Pizza Carbonara">Pizza Carbonara</option>
                                    <option class="text-center" value="Pizza KA">Pizza Kebab Agneau</option>
                                    <option class="text-center" value="Pizza KP">Pizza Kebab Poulet</option>
                                    <option class="text-center" value="Pizza Parma">Pizza Parma</option>
                                    <option class="text-center" value="Pizza HopZop">Pizza HipZop</option>
                                    <option class="text-center" value="Pizza Capricciosa">Pizza Capricciosa</option>
                                    <option class="text-center" value="Pizza Champignons">Pizza Champignons</option>
                                    <option class="text-center" value="Pizza Lardee">Pizza Lardée</option>
                                    <option class="text-center" value="Pizza Lardee Royal">Pizza Lardée Royal</option>
                                    <option class="text-center" value="Pizza Crottine">Pizza Crottine</option>
                                    <option class="text-center" value="Pizza Basilic">Pizza Basilic</option>
                                    <option class="text-center" value="Pizza Jungle">Pizza de la Jungle</option>
                                    <option class="text-center" value="Pizza Arrabiata">Pizza Arrabiata</option>
                                    <option class="text-center" value="Pizza Fefe">Pizza Féfé</option>
                                </select>


                            </div>
                            <div class="col-md-6"><p>Taille</p></div>
                            <div class="col-md-6 " style="">

                                <select class="form-control" id="taillepizzachoice" onchange="loadprice()">
                                    <option class="text-center" value="32">32 cm</option>
                                    <option class="text-center" value="40">40 cm</option>
                                    <option class="text-center" value="45">45 cm</option>
                                    <option class="text-center" value="4545">45x45 cm</option>
                                </select>

                            </div>
                            <div class="col-md-6 " style="">
                                <h3 id="prixpizzachoice">17 CHF</h3>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezmenuchoicepizza"> Commandez</button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <div class="col-md-12" style="margin-top: 1cm" id="cid-Burgers" >
                <h2>Burgers</h2>
                <hr>
                <div class="card">
                    <img src="assets/images/burger2.jpg" alt="Avatar" style="width:100%" >
                    <div class="container">
                        <h4>Menu Burgers</h4>
                        <p>Obtenir votre Burgers</p>

                    </div>
                    <div class="container"  id="menuchoiceBurgers">
                        <div class="row"   style="margin-top: 1cm;" >
                            <div class="col-md-6"><p>Burgers </p></div>
                            <div class="col-md-6 " style="">


                                <select class="form-control"  id="Burgerschoice"  onchange="loadBurgers()">
                                    <option class="text-center" value="Hamburger">Hamburger(80g)</option>
                                    <option class="text-center" value="BigCityBurger et pommes frites">Big City Burger et pommes frites</option>
                                    <option class="text-center" value="MaisonBurger">Maison Burger (160g)</option>
                                    <option class="text-center" value="Cheeseburger">Cheeseburger (80g)</option>
                                    <option class="text-center" value="DoubleBurgerOeuf">Double Burger Oeuf (160g)
                                    </option>
                                    <option class="text-center" value="DoubleBurger">Hamburger</option>
                                    <option class="text-center" value="DoubleCheeseburgerOeuf">Double Cheeseburger Oeuf (160g)
                                    </option>
                                    <option class="text-center" value="DoubleCheeseburger">Double Cheeseburger (160g)</option>
                                    <option class="text-center" value="BIGBurger">BIG Burger (240g)</option>
                                    <option class="text-center" value="BIGCheeseburger">BIG Cheeseburger (240g)</option>
                                    <option class="text-center" value="CrèveLaDalle">Crève La Dalle (320g)</option>
                                    <option class="text-center" value="LingLingBurger">Ling-Ling Burger (240g)</option>
                                    <option class="text-center" value="ClassicBurger">Classic Burger (240g)</option>
                                    <option class="text-center" value="RonaldoBurger">Ronaldo Burger (240g)</option>
                                    <option class="text-center" value="MilanoBurger">Milano Burger (240g)</option>
                                    <option class="text-center" value="HamburgerGruyère">Hamburger Gruyère (160g)</option>
                                    <option class="text-center" value="HamburgerRaclette">Hamburger Raclette (160g)</option>
                                    <option class="text-center" value="HamburgerTroisFromages">Hamburger Trois Fromages (160g)</option>
                                    <option class="text-center" value="Route66Burger">Route 66 Burger (240g)</option>
                                    <option class="text-center" value="BurgerTorooo">Burger Torooo (240g)</option>
                                    <option class="text-center" value="CampagnardBurger">Campagnard Burger (240g)</option>
                                    <option class="text-center" value="BilkessBurger">Bilkess Burger (160g)</option>
                                    <option class="text-center" value="OassasBurger">Oassas Burger (160g)</option>
                                    <option class="text-center" value="VeveysanBurger">Veveysan Burger (160g)</option>
                                    <option class="text-center" value="RivieraBurger">Riviera Burger (160g)</option>
                                    <option class="text-center" value="TutédrakuBurger">Tutédraku Burger (320g)</option>
                                    <option class="text-center" value="ChorizoBurger">Chorizo Burger (240g)</option>
                                    <option class="text-center" value="JambonBurger">Jambon Burger (240g)</option>
                                    <option class="text-center" value="LeMagic">Le Magic</option>
                                    <option class="text-center" value="LePaysan">Le Paysan</option>
                                    <option class="text-center" value="ElPasha">El Pasha</option>
                                    <option class="text-center" value="Cocorico">Cocorico</option>
                                    <option class="text-center" value="BurgerCordonBleu">Burger Cordon Bleu</option>
                                    <option class="text-center" value="FalafelBurger">Falafel Burger</option>


                                </select>



                            </div>

                            <div class="col-md-6 " style="">
                                <p id="BurgersDescription">Viande (80g), salade, oignons, tomates, ketchup et mayonnaise</p>

                                <h3 id="prixBurgerschoice">6.00 CHF</h3>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezmenuchoiceBurgers">  Commandez </button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <div style="margin-top: 1cm;" class="col-md-12"  id="cid-Panini" >
                <h2>Panini</h2>
                <hr>

            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/panini2.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Panini</h4>
                        <p>choisissez votre panini et selectionnez les ingrédients</p>

                    </div>
                    <div class="container" id="menusandwichkebab">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalpanini">Total : 10.00 CHF</p>
                                <p class="col-md-12" id="totall2panini" style="visibility: hidden">10.00</p>


                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="panini">
                                    <option class="text-center" value="Panini Agneau">Panini Agneau</option>
                                    <option class="text-center" value="Panini Poulet">Panini Poulet</option>
                                    <option class="text-center" value="Panini Chorizo">Panini Chorizo</option>
                                    <option class="text-center" value="Panini viande séche">Panini viande séche</option>
                                    <option class="text-center" value="Panini Fromage">Panini Fromage</option>
                                    <option class="text-center" value="Panini jambon">Panini jambon</option>
                                    <option class="text-center" value="Panini Tomate Mozzarella">Panini Tomate Mozzarella</option>
                                    <option class="text-center" value="Panini Mixte">Panini Mixte</option>

                                </select>

                            </div>

                            <div class="row" style="margin-top: 1cm;" id="garniturespan">
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Salade" value="Salade" class="garniturespanini">
                                    <label>Salade 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Oignon" value="Oignon" class="garniturespanini">
                                    <label>Oignon 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Tomate" value="Tomate" class="garniturespanini">
                                    <label>Tomate 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Saucepiquant" value="Saucepiquant" class="garniturespanini">
                                    <label>Sauce piquant 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucecocktail" value="saucecocktail" class="garniturespanini">
                                    <label>Sauce Cocktail 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucealgerienne" value="saucealgerienne" class="garniturespanini">
                                    <label>Sauce algérienne 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="sauceblanche" value="sauceblanche" class="garniturespanini">
                                    <label>Sauce blanche 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucetartare" value="saucetartare" class="garniturespanini">
                                    <label>Sauce piquant 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucecurry" value="saucecurry" class="garniturespanini">
                                    <label>Sauce Curry 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucemayonnaise" value="saucemayonnaise" class="garniturespanini">
                                    <label>Sauce mayonnaise 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="sauceketchup" value="sauceketchup" class="garniturespanini">
                                    <label>Sauce ketchup 0.00</label>
                                </div>



                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezpanini"> Commandez</button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

            <div class="col-md-12"  style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/pizzabackground1.jpg" id="img1" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Pizza Marguerite</h4>
                        <p>Vous obtenez une pizza Marguerite avec vos garniture à choix</p> <br>

                    </div>
                    <div class="container" id="menugar">
                        <div class="row" style="margin-top: 1cm;">

                            <p class="col-md-12" id="total">Total : 17.00 CHF</p>
                            <p class="col-md-12" id="totall2" style="visibility: hidden">17.00</p>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="taille">
                                    <option class="text-center" value="32">32 cm 17.00</option>
                                    <option class="text-center" value="40">40 cm 32.00</option>
                                    <option class="text-center" value="45">45 cm 39.00</option>
                                    <option class="text-center" value="4545">45x45 cm 52.00</option>
                                </select>

                            </div>


                        </div>
                        <div class="row" style="margin-top: 1cm;">
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Ail"  class="garniture">
                                <label>Ail 1.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Ananas"  class="garniture">
                                <label>Ananas 2.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Anchois"  class="garniture">
                                <label>Anchois 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Aubergine"  class="garniture">
                                <label>Aubergine 1.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Basilique"  class="garniture">
                                <label>Basilique 1.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Champignon"  class="garniture">
                                <label>Champignon 2.00</label>
                            </div>

                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Chèvre"  class="garniture">
                                <label>Chèvre 2.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="ChampignondeParisfrais"  class="garniture">
                                <label>Champignon de Paris frais 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Chorizo"  class="garniture">
                                <label>Chorizo 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Courgette"  class="garniture">
                                <label>Courgette 1.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Crevette"  class="garniture">
                                <label> Crevette 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Émincédepoulet"  class="garniture">
                                <label> Émincé de poulet 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Fruitsdemer"  class="garniture">
                                <label> Fruits de mer 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Gorgonzola"  class="garniture">
                                <label>Gorgonzola 2.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Granapodano"  class="garniture">
                                <label> Grana podano 2.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Gruyère"  class="garniture">
                                <label>Gruyère 2.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Kebab"  class="garniture">
                                <label>  Kebab 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Agneau"  class="garniture">
                                <label>Agneau 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Poulet"  class="garniture">
                                <label> Poulet 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="AgneauPoulet"  class="garniture">
                                <label> Agneau Poulet 5.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Lardons"  class="garniture">
                                <label>Lardons 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Légumesgrillés"  class="garniture">
                                <label> Légumes grillés 3.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Mozzarella"  class="garniture">
                                <label>Mozzarella 2.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Oignon"  class="garniture">
                                <label>Oignon 1.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Salade"  class="garniture">
                                <label>Salade 1.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Maïs"  class="garniture">
                                <label>Maïs 2.00</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Poivrons"  class="garniture">
                                <label> Poivrons 1.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Frites"  class="garniture">
                                <label>Frites 2.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Rucola"  class="garniture">
                                <label>Rucola 1.50</label>
<<<<<<< Commande.php
=======
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Salami"  class="garniture">
                                <label> Salami 3.00</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandez"> Commandez</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/pizzabackground1.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Menu pizza</h4>
                        <p>Obtenir votre pizza</p>

                    </div>
                    <div class="container" id="menuchoicepizza">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="col-md-6"><p>Pizza </p></div>
                            <div class="col-md-6 " style="">

                                <select class="form-control" id="pizzachoice" onchange="loadpizza()">
                                    <option class="text-center"  value="merguez">Pizza merguez</option>
                                    <option class="text-center" value="Pizza margerite">Pizza margerite</option>
                                    <option class="text-center" value="Pizza Thon">Pizza Thon</option>
                                    <option class="text-center" value="Pizza Thai">Pizza Thai</option>
                                    <option class="text-center" value="Pizza Romana">Pizza Romana</option>
                                    <option class="text-center" value="Pizza 4 Saisons">Pizza 4 saisons</option>
                                    <option class="text-center" value="Pizza Crevettes">Pizza Crevettes</option>
                                    <option class="text-center" value="Pizza Fruits De Mer">Pizza Fruits De Mer</option>
                                    <option class="text-center" value="Pizza Sucuk">Pizza Sucuk (saucisse turc)</option>
                                    <option class="text-center" value="Pizza Royal">Pizza Royal</option>
                                    <option class="text-center" value="Pizza Jambon">Pizza Jambon</option>
                                    <option class="text-center" value="Pizza Diavola">Pizza Diavola</option>
                                    <option class="text-center" value="Pizza Hawai">Pizza Hawai</option>
                                    <option class="text-center" value="Pizza Napoli">Pizza Napoli</option>
                                    <option class="text-center" value="Pizza Vegetarienne">Pizza Végétarienne</option>
                                    <option class="text-center" value="Pizza Tunisienne">Pizza Tunisienne</option>
                                    <option class="text-center" value="Pizza Quatre Fromages">Pizza Quatre Fromages</option>
                                    <option class="text-center" value="Pizza Calzona">Pizza Calzona</option>
                                    <option class="text-center" value="Pizza Carbonara">Pizza Carbonara</option>
                                    <option class="text-center" value="Pizza KA">Pizza Kebab Agneau</option>
                                    <option class="text-center" value="Pizza KP">Pizza Kebab Poulet</option>
                                    <option class="text-center" value="Pizza Parma">Pizza Parma</option>
                                    <option class="text-center" value="Pizza HopZop">Pizza HipZop</option>
                                    <option class="text-center" value="Pizza Capricciosa">Pizza Capricciosa</option>
                                    <option class="text-center" value="Pizza Champignons">Pizza Champignons</option>
                                    <option class="text-center" value="Pizza Lardee">Pizza Lardée</option>
                                    <option class="text-center" value="Pizza Lardee Royal">Pizza Lardée Royal</option>
                                    <option class="text-center" value="Pizza Crottine">Pizza Crottine</option>
                                    <option class="text-center" value="Pizza Basilic">Pizza Basilic</option>
                                    <option class="text-center" value="Pizza Jungle">Pizza de la Jungle</option>
                                    <option class="text-center" value="Pizza Arrabiata">Pizza Arrabiata</option>
                                    <option class="text-center" value="Pizza Fefe">Pizza Féfé</option>
                                </select>


                            </div>
                            <div class="col-md-6"><p>Taille</p></div>
                            <div class="col-md-6 " style="">

                                <select class="form-control" id="taillepizzachoice" onchange="loadprice()">
                                    <option class="text-center" value="32">32 cm</option>
                                    <option class="text-center" value="40">40 cm</option>
                                    <option class="text-center" value="45">45 cm</option>
                                    <option class="text-center" value="4545">45x45 cm</option>
                                </select>

                            </div>
                            <div class="col-md-6 " style="">
                                <h3 id="prixpizzachoice">17 CHF</h3>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezmenuchoicepizza"> Commandez</button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
           
            
            <div class="col-md-12" style="margin-top: 1cm;" id="cid-PetiteFaim">
                <h2>Petite Faim</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/pizzabackground1.jpg" id="img1" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Petite Faim</h4>
                        <p></p>
                    </div>
                    <div class="container" id="menugarpetitfaim">
                        <div class="row" style="margin-top: 1cm;">

                            <p class="col-md-12" id="totalpetitfaim">Total : 6.00 CHF</p>
                            <p class="col-md-12" id="totall2petitfaim" style="visibility: hidden">6.00</p>


                        </div>
                        <div class="col-md-12 " style="">

                            <select class="form-control" id="typepetitfaim">
                                <option class="text-center" value="Petiteportionfrites">Petite portion frites  6.00 CHF</option>
                                <option class="text-center" value="Grandeportionfrites">Grande portion frites  11.00 CHF </option>
                                <option class="text-center" value="ChickenNuggets6pièces">Chicken Nuggets (6 pièces) 8.00 CHF</option>
                                <option class="text-center" value="ChickenNuggets12pièces">Chicken Nuggets (12 pièces) 15.00 CHF</option>
                                <option class="text-center" value="ChickenNuggets18pièces">Chicken Nuggets (18 pièces) 21.00 CHF</option>
                                <option class="text-center" value="ChickenNuggets24pièces">Chicken Nuggets (24 pièces) 28.00 CHF</option>
                                <option  class="text-center"  value="sandwichfrites" >Sandwich frites 8.00</option>
                                <option  class="text-center"  value="sandwichchorizo"> Sandwich chorizo (froid) 8.00</option>
                                <option  class="text-center"  value="sandwichjambon"> Sandwich jambon (froid) 8.00</option>
                                <option  class="text-center"  value="sandwichfromage">Sandwich fromage (froid) 8.00</option>
                                <option  class="text-center"  value="Sandwichthonmayonnaise">Sandwich thon mayonnaise (froid) 8.00</option>

                            </select>

                        </div>
                        <div class="row" style="margin-top: 1cm;" id="garniturepetitfaim">
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="sauceblanche" value="sauceblanche" class="garniturepetitfaim">
                                <label>sauce blanche 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="saucealgérienne" value="saucealgérienne" class="garniturepetitfaim">
                                <label>sauce algérienne 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="saucesamouraï" value="saucesamouraï" class="garniturepetitfaim">
                                <label>sauce samouraï 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="saucetartare" value="saucetartare" class="garniturepetitfaim">
                                <label>sauce tartare 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucebigiburger" value="saucebigiburger" class="garniturepetitfaim">
                                <label>sauce bigi-burger 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucebarbecue" value="saucebarbecue" class="garniturepetitfaim">
                                <label> sauce barbecue 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucecocktail" value="saucecocktail" class="garniturepetitfaim">
                                <label> sauce cocktail 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucemarocaine" value="saucemarocaine" class="garniturepetitfaim">
                                <label> sauce marocaine 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucecurry" value="saucecurry" class="garniturepetitfaim">
                                <label> sauce curry 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucealail" value="saucealail" class="garniturepetitfaim">
                                <label> sauce à l'ail 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="sauceandalouse" value="sauceandalouse" class="garniturepetitfaim">
                                <label> sauce andalouse 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="mayonnaise" value="mayonnaise" class="garniturepetitfaim">
                                <label> mayonnaise 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="ketchup" value="ketchup" class="garniturepetitfaim">
                                <label>Ketchup 0.50</label>
                            </div>



                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezpetitfaim"> Commandez</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div style="margin-top: 1cm;" class="col-md-12" id="cid-Kebab">
                <h2>Kebab & Sandwich</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/kabebetsandwitch.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Kebab & Sandwich</h4>
                        <p>choisissez votre sandwich ou votre kebab  et selectionnez les ingrédients</p>

                    </div>
                    <div class="container" id="menusandwichkebab">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalsandwichkabeb">Total : 12.00 CHF</p>
                                <p class="col-md-12" id="totall2sandwichkabeb" style="visibility: hidden">12.00</p>


                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="sandwichkebab">
                                    <option class="text-center" value="DürümKebabagneau">Dürüm Kebab agneau</option>
                                    <option class="text-center" value="DürümKebabpoulet">Dürüm Kebab poulet</option>
                                    <option class="text-center" value="DürümKebabagneauetpouletmixte">Dürüm Kebab agneau et poulet mixte</option>
                                    <option class="text-center" value="DürümKöfte">Dürüm Köfte</option>
                                    <option class="text-center" value="DürümFalafel">Dürüm Falafel</option>
                                    <option class="text-center" value="SandwichKebabagneau">Sandwich Kebab agneau</option>
                                    <option class="text-center" value="SandwichKebabpoulet">Sandwich Kebab poulet</option>
                                    <option class="text-center" value="SandwichKebabagneauetpouletmixte">Sandwich Kebab agneau et poulet mixte</option>
                                    <option class="text-center" value="Sandwichgigotdagneau">Sandwich gigot d'agneau</option>
                                    <option class="text-center" value="Sandwichpouletgrillé">Sandwich poulet grillé</option>

                                </select>

>>>>>>> Commande.php
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" value="Salami"  class="garniture">
                                <label> Salami 3.00</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandez"> Commandez</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="col-md-12" style="margin-top: 1cm" id="cid-Box">
                <h2>Box</h2>
                <hr>
                <div class="card">
                    <img src="assets/images/box.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Box</h4>
                        <p>choisissez votre Box et selectionnez les ingrédients</p>

                    </div>
                    <div class="container" id="menuBox">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalBox">Total : 13.00 CHF</p>
                                <p class="col-md-12" id="totall2Box" style="visibility: hidden">10.00</p>


                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="typeBox">
                                    <option class="text-center" value="BoxKebab">Box Mixte</option>
                                    <option class="text-center" value="BoxPoulet">Box Poulet</option>
                                    <option class="text-center" value="BoxAgneau">Box Agneau</option>
                                    <option class="text-center" value="BoxFalafls">Box Falafels</option>
                                    <option class="text-center" value="BoxNuggets">Box Nuggets</option>
                                    <option class="text-center" value="BoxCalamar">Box Calamar</option>



                                </select>

                            </div>
                            <div class="row" style="margin-top: 1cm;" id="garniturestacoss">


                                <h3>-liste des sauces :</h3><br>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Algérienne" value="Algérienne" class="garnituresbox">
                                    <label>Algérienne 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Andalouse" value="Andalouse" class="garnituresbox">
                                    <label> Andalouse 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Ketchup" value="Ketchup" class="garnituresbox">
                                    <label>Ketchup  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Lail" value="Lail" class="garnituresbox">
                                    <label>L’ail 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Marocaine" value="Marocaine" class="garnituresbox">
                                    <label>Marocaine 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Samouraï" value="Samouraï" class="garnituresbox">
                                    <label>Samouraï 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Mayonnaise" value="Mayonnaise" class="garnituresbox">
                                    <label>Mayonnaise 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="BiggiBurger" value="BiggiBurger" class="garnituresbox">
                                    <label>Biggi-Burger  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="barbecue" value="barbecue" class="garnituresbox">
                                    <label>barbecue  0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Cocktail" value="Cocktail" class="garnituresbox">
                                    <label>Cocktail 0.00</label>
                                </div>


                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Brasil" value="Brasil" class="garnituresbox">
                                    <label>Brasil 0.00</label>
                                </div>

                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezassiette"> Commandez</button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <div style="margin-top: 1cm;" class="col-md-12" id="cid-Salades" >
                <h2>Salades</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/pizzabackground1.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Salades</h4>
                        <p>choisissez votre salade  et selectionnez votre sauce</p>

                    </div>
                    <div class="container">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalsalade">Total : 5.00 CHF</p>



                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="Salades">
                                    <option class="text-center" value="Salade verte">Salade verte</option>
                                    <option class="text-center"  value="Salade mixte">Salade mixte</option>
                                    <option class="text-center"  value="Salade mêlée">Salade mêlée</option>
                                    <option class="text-center" value="Salade grecque">Salade grecque</option>
                                    <option class="text-center" value="Salade César">Salade César</option>
                                    <option class="text-center" value="Salade au thon">Salade au thon</option>



                                </select>

                            </div>

                            <div class="col-md-12 " style="">

                                <select class="form-control" id="typesalade">
                                    <option class="text-center" value="sauce française">sauce française</option>
                                    <option class="text-center" value="sauce italienne">sauce italienne</option>



                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Curry" value="Curry" class="garnituresbox">
                                    <label>Curry 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Tartare" value="Tartare" class="garnituresbox">
                                    <label>Tartare 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Blanche" value="Blanche" class="garnituresbox">
                                    <label>Blanche 0.00</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="Américaine" value="Américaine" class="garnituresbox">
                                    <label>Américaine 0.00</label>
                                </div>

                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezBox"> Commandez</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div style="margin-top: 1cm;" class="col-md-12"  id="cid-Pide" >
                <h2>Pide</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/pide2.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Pide</h4>
                        <p>choisissez votre pide  et selectionnez les ingrédients</p>
                        <p>(Servi avec une petite salade)</p>

                    </div>
                    <div class="container">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalpide">Total : 16.00	 CHF</p>



                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="pide">
                                    <option class="text-center" value="Pide au fromage">Pide au fromage</option>
                                    <option class="text-center" value="Pide à la viande hachée">Pide à la viande hachée</option>
                                    <option class="text-center" value="Pide au sucuk">Pide au sucuk</option>
                                    <option class="text-center" value="Pide au sucuk et oeuf">Pide au sucuk et oeuf</option>
                                    <option class="text-center" value="Pide aux épinards">Pide aux épinards</option>
                                    <option class="text-center" value="Pide aux épinards et oeuf">Pide aux épinards et oeuf</option>
                                    <option class="text-center" value="Pide kebab agneau">Pide kebab agneau</option>
                                    <option class="text-center" value="Pide kebab poulet">Pide kebab poulet</option>
                                    <option class="text-center" value="Pide avec un oeuf">Pide avec un oeuf</option>
                                    <option class="text-center" value="Pide merguez et oeuf">Pide merguez et oeuf</option>





                                </select>

                            </div>

                            <div class="col-md-12 " style="">

                                <select class="form-control" id="typepide">
                                    <option class="text-center" value="sauce française">sauce française</option>
                                    <option class="text-center" value="sauce italienne">sauce italienne</option>


                                </select>

                            </div>


                            <div class="row" style="margin-top: 0.5cm;">
                                <div class="col-md-6 col-md-offset-3">
                                    <button class="btn btn-success" id="commandezpide"> Commandez</button>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <div class="col-md-12"  style="margin-top: 1cm" id="cid-Assiettes">
                <h2>Assiette</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/assiette.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Assiette</h4>
                        <p>choisissez votre Assiette  et selectionnez les ingrédients</p>

                    </div>
                    <div class="container" id="menuassiette">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalassiette">Total : 22.00 CHF</p>
                                <p class="col-md-12" id="totall2assiette" style="visibility: hidden">17.00</p>


                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="assiette">
                                    <option class="text-center" value="AssietteBrochettedepouletavecfritesetsalade">Assiette Brochette de poulet avec frites et salade</option>
                                    <option class="text-center" value="Assiette Brochette de poulet avec riz et salade">Assiette Brochette de poulet avec riz et salade</option>
                                    <option class="text-center" value="Assiette brochette d'agneau avec frites et salade">Assiette brochette d'agneau avec frites et salade</option>
                                    <option class="text-center" value="Assiette brochette d'agneau avec riz et salade">Assiette brochette d'agneau avec riz et salade</option>
                                    <option class="text-center" value="Assiette grillade mixte avec frites et salade">Assiette grillade mixte avec frites et salade</option>
                                    <option class="text-center" value="Assiette grillade mixte avec riz et salade">Assiette grillade mixte avec riz et salade</option>
                                    <option class="text-center" value="Assiette falafel avec frites et salade">Assiette falafel avec frites et salade</option>
                                    <option class="text-center" value="Assiette falafel avec riz et salade">Assiette falafel avec riz et salade</option>
                                    <option class="text-center" value="Assiette kebab d'agneau avec frites et salade">Assiette kebab d'agneau avec frites et salade</option>
                                    <option class="text-center" value="Assiette kebab d'agneau avec riz et salade">Assiette kebab d'agneau avec riz et salade</option>

                                    <option class="text-center" value="Assiette steak chicken avec frites et salade">Assiette steak chicken avec frites et salade</option>

                                    <option class="text-center" value="Assiette steak chicken avec riz et salade">Assiette steak chicken avec riz et salade</option>
                                    <option class="text-center" value="Assiette cordon bleu avec frites et salade">Assiette cordon bleu avec frites et salade</option>
                                    <option class="text-center" value="Assiette cordon bleu avec riz et salade">Assiette cordon bleu avec riz et salade</option>
                                    <option class="text-center" value="Assiette calamars avec frites et salade">Assiette calamars avec frites et salade</option>
                                    <option class="text-center" value="Assiette calamars avec riz et salade">Assiette calamars avec riz et salade</option>
                                    <option class="text-center" value="Assiette ail de poulet grillé avec salade et pommes frites">Assiette ail de poulet grillé avec salade et pommes frites</option>
                                    <option class="text-center" value="Assiette ail de poulet grillé avec salade et riz">Assiette ail de poulet grillé avec salade et riz</option>
                                    <option class="text-center" value="Assiette köfte avec riz et salade">Assiette köfte avec riz et salade</option>
                                    <option class="text-center" value="Assiette köfte avec frites et salade">Assiette köfte avec frites et salade</option>
                                    <option class="text-center" value="Assiette côtelette d'agneau avec riz et salade">Assiette côtelette d'agneau avec riz et salade</option>
                                    <option class="text-center" value="Assiette côtelette d'agneau avec frites et salade">Assiette côtelette d'agneau avec frites et salade</option>
                                    <option class="text-center" value="Assiette kebab poulet avec frites et salade">Assiette kebab poulet avec frites et salade</option>
                                    <option class="text-center" value="Assiette kebab mixte avec frites et salade">Assiette kebab mixte avec frites et salade</option>
                                    <option class="text-center" value="Assiette Adana avec riz et salade">Assiette Adana avec riz et salade</option>
                                    <option class="text-center" value="Assiette Adana avec pommes frites et salade">Assiette Adana avec pommes frites et salade</option>
                                    <option class="text-center" value="Assiette côtelette avec riz et salade">Assiette côtelette avec riz et salade</option>
                                    <option class="text-center" value="Assiette côtelette avec frites et salade">Assiette côtelette avec pommes frites et salade</option>
                                    <option class="text-center" value="Assiette Merguez avec riz et salade">Assiette Merguez avec riz et salade</option>
                                    <option class="text-center" value="Assiette Merguez avec pommes frites et salade">Assiette Merguez avec pommes frites et salade</option>
                                    <option class="text-center" value="Assiette Lahmacun avec riz et salade">Assiette Lahmacun avec riz et salade</option>
                                    <option class="text-center" value="Assiette Lahmacun avec pommes frites et salade">Assiette Lahmacun avec pommes frites et salade</option>




                                </select>

                            </div>
                            <div class="row" style="margin-top: 1cm;" id="garnitureassiette">
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="supplémentpain" value="supplémentpain" class="garnitureass">
                                    <label>supplément pain 1.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="sauceblanche" value="sauceblanche" class="garnitureass">
                                    <label>sauce blanche 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucealgérienne" value="saucealgérienne" class="garnitureass">
                                    <label>sauce algérienne 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucesamouraï" value="saucesamouraï" class="garnitureass">
                                    <label>sauce samouraï 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" autocomplete="off" id="saucetartare" value="saucetartare" class="garnitureass">
                                    <label>sauce tartare 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucefrancaise" value="saucefrancaise" class="garnitureass">
                                    <label>sauce française 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="sauceketchup" value="sauceketchup" class="garnitureass">
                                    <label>sauce ketchup 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="sauceitalienne" value="sauceitalienne" class="garnitureass">
                                    <label>sauce italienne 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucecocktail" value="saucecocktail" class="garnitureass">
                                    <label>sauce cocktail 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucemarocaine" value="saucemarocaine" class="garnitureass">
                                    <label>sauce marocaine 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucecurry" value="saucecurry" class="garnitureass">
                                    <label>sauce curry 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucealail" value="saucealail" class="garnitureass">
                                    <label>sauce à l'ail 0.00</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="saucemayonnaise" value="saucemayonnaise" class="garnitureass">
                                    <label>sauce mayonnaise 0.00</label>
                                </div>



                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezassiette"> Commandez</button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <div style="margin-top: 1cm;" class="col-md-12" id="cid-Salades" >
                <h2>Salades</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/salade2.jpg" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Salades</h4>
                        <p>choisissez votre salade  et selectionnez votre sauce</p>

                    </div>
                    <div class="container">
                        <div class="row" style="margin-top: 1cm;">
                            <div class="row" style="margin-top: 1cm;">

                                <p class="col-md-12" id="totalsalade">Total : 5.00 CHF</p>



                            </div>
                            <div class="col-md-12 " style="">

                                <select class="form-control" id="salade">
                                    <option class="text-center" value="Salade verte">Salade verte</option>
                                    <option class="text-center" value="Salade mixte">Salade mixtee</option>
                                    <option class="text-center" value="Salade mêlée">Salade mêlée</option>
                                    <option class="text-center" value="Salade grecque">Salade grecque</option>
                                    <option class="text-center" value="Salade César">Salade César</option>
                                    <option class="text-center" value="Salade au thon">Salade au thon</option>



                                </select>

                            </div>

                            <div class="col-md-12 " style="">

                                <select class="form-control" id="typesalade">
                                    <option class="text-center" value="sauce française">sauce française</option>
                                    <option class="text-center" value="sauce italienne">sauce italienne</option>


                                </select>

                            </div>


                            <div class="row" style="margin-top: 0.5cm;">
                                <div class="col-md-6 col-md-offset-3">
                                    <button class="btn btn-success" id="commandezsalade"> Commandez</button>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 1cm;" id="cid-PetiteFaim">
                <h2>Petite Faim</h2>
                <hr>
            </div>
            <div class="col-md-12" style="margin-top: 1cm">
                <div class="card">
                    <img src="assets/images/faim2.jpg" id="img1" alt="Avatar" style="width:100%">
                    <div class="container">
                        <h4>Petite Faim</h4>
                        <p></p>
                    </div>
                    <div class="container" id="menugarpetitfaim">
                        <div class="row" style="margin-top: 1cm;">

                            <p class="col-md-12" id="totalpetitfaim">Total : 6.00 CHF</p>
                            <p class="col-md-12" id="totall2petitfaim" style="visibility: hidden">17.00</p>


                        </div>
                        <div class="col-md-12 " style="">

                            <select class="form-control" id="typepetitfaim">
                                <option class="text-center" value="Petiteportionfrites">Petite portion frites  6.00 CHF</option>
                                <option class="text-center" value="Grandeportionfrites">Grande portion frites  11.00 CHF </option>
                                <option class="text-center" value="ChickenNuggets6pièces">Chicken Nuggets (6 pièces) 8.00 CHF</option>
                                <option class="text-center" value="ChickenNuggets12pièces">Chicken Nuggets (12 pièces) 15.00 CHF</option>
                                <option class="text-center" value="ChickenNuggets18pièces">Chicken Nuggets (18 pièces) 21.00 CHF</option>
                                <option class="text-center" value="ChickenNuggets24pièces">Chicken Nuggets (24 pièces) 28.00 CHF</option>
                                <option  class="text-center"  value="sandwichfrites" >Sandwich frites 8.00</option>
                                <option  class="text-center"  value="sandwichchorizo"> Sandwich chorizo (froid) 8.00</option>
                                <option  class="text-center"  value="sandwichjambon"> Sandwich jambon (froid) 8.00</option>
                                <option  class="text-center"  value="sandwichfromage">Sandwich fromage (froid) 8.00</option>
                                <option  class="text-center"  value="Sandwichthonmayonnaise">Sandwich thon mayonnaise (froid) 8.00</option>

                            </select>

                        </div>
                        <div class="row" style="margin-top: 1cm;" id="garniturepetitfaim">
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="sauceblanche" value="sauceblanche" class="garniturepetitfaim">
                                <label>sauce blanche 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="saucealgérienne" value="saucealgérienne" class="garniturepetitfaim">
                                <label>sauce algérienne 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="saucesamouraï" value="saucesamouraï" class="garniturepetitfaim">
                                <label>sauce samouraï 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" autocomplete="off" id="saucetartare" value="saucetartare" class="garniturepetitfaim">
                                <label>sauce tartare 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucebigiburger" value="saucebigiburger" class="garniturepetitfaim">
                                <label>sauce bigi-burger 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucebarbecue" value="saucebarbecue" class="garniturepetitfaim">
                                <label> sauce barbecue 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucecocktail" value="saucecocktail" class="garniturepetitfaim">
                                <label> sauce cocktail 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucemarocaine" value="saucemarocaine" class="garniturepetitfaim">
                                <label> sauce marocaine 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucecurry" value="saucecurry" class="garniturepetitfaim">
                                <label> sauce curry 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="saucealail" value="saucealail" class="garniturepetitfaim">
                                <label> sauce à l'ail 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="sauceandalouse" value="sauceandalouse" class="garniturepetitfaim">
                                <label> sauce andalouse 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="mayonnaise" value="mayonnaise" class="garniturepetitfaim">
                                <label> mayonnaise 0.50</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="ketchup" value="ketchup" class="garniturepetitfaim">
                                <label>Ketchup 0.50</label>
                            </div>



                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezpetitfaim"> Commandez</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>






            <div class="col-md-12" style="margin-top: 1cm" id="cid-Boissons">
                <h2>Boissons</h2>
                <hr>
                <div class="card">
                    <img src="assets/images/pizzabackground1.jpg" alt="Avatar" style="width:100%" >
                    <div class="container">
                        <h4>Menu Boissons</h4>
                        <p>Obtenir votre Boissons</p>

                    </div>
                    <div class="container"  id="menuchoiceBoissons">
                        <div class="row"   style="margin-top: 1cm;" >
                            <div class="col-md-6"><p>Boissons </p></div>
                            <div class="col-md-6 " style="">

                                <select class="form-control"  id="Boissonschoices"  onchange="loadBoissons()">
                                    <option class="text-center" value="Eau minérale gazeuse0.5">Eau minérale gazeuse 0.5L
                                    </option>
                                    <option class="text-center" value="Eau minérale gazeuse1.5">Eau minérale gazeuse 1.5L
                                    </option>
                                    <option class="text-center" value="Eau minérale naturelle0.5">Eau minérale naturelle 0.5L
                                    </option>
                                    <option class="text-center" value="Eau minérale naturelle1.5">Eau minérale naturelle 1.5L
                                    </option>
                                    <option class="text-center" value="Coca Cola0.33">Coca Cola 0.33L
                                    </option>
                                    <option class="text-center" value="Coca Cola0.5">Coca Cola 0.5L
                                    </option>
                                    <option class="text-center" value="Coca Cola1.5">Coca Cola 1.5L
                                    </option>
                                    <option class="text-center" value="Coca Cola Zero0.5">Coca Cola Zero 0.5L
                                    </option>
                                    <option class="text-center" value="Coca Cola Zero1.5">Coca Cola Zero 1.5L
                                    </option>
                                    <option class="text-center" value="Fanta Orange0.5">Fanta Orange 0.5L
                                    </option>
                                    <option class="text-center" value="Fanta Orange1.5">Fanta Orange 1.5L
                                    </option>
                                    <option class="text-center" value="Fanta Shokota">Fanta Shokota 0.5L
                                    </option>
                                    <option class="text-center" value="Fanta Lémon">Fanta Lémon 0.33L
                                    </option>
                                    <option class="text-center" value="Fanta Fraise Kiwi">Fanta Fraise Kiwi 0.33L
                                    </option>
                                    <option class="text-center" value="Fanta Exotique">Fanta Exotique 0.33L
                                    </option>
                                    <option class="text-center" value="Sprite0.5">Sprite 0.5L</option>
                                    <option class="text-center" value="Sprite1.5">Sprite 1.5L</option>

                                    <option class="text-center" value="Thé Froid Peach0.5">Thé Froid Peach 0.5L
                                    </option>
                                    <option class="text-center" value="Thé Froid Peach1.5">Thé Froid Peach 1.5L
                                    </option>
                                    <option class="text-center" value="Thé Froid Lemon0.5">Thé Froid Lemon 0.5L
                                    </option>
                                    <option class="text-center" value="Thé Froid Lemon1.5">Thé Froid Lemon 1.5L
                                    </option>
                                    <option class="text-center" value="Uludag Orange">Uludag Orange 0.5L
                                    </option>
                                    <option class="text-center" value="Ayran">Ayran 0.25L</option>
                                    <option class="text-center" value="Red Bull">Red Bull 0.355L
                                    </option>
                                    <option class="text-center" value="Arizona thé vert">Arizona thé vert 0.5L
                                    </option>
                                    <option class="text-center" value="Oishi Thé vert Citron Miel">Oishi Thé vert Citron Miel 0.5L
                                    </option>
                                    <option class="text-center" value="Jus d'orange">Jus d'orange 1.00L
                                    </option>
                                    <option class="text-center" value="Jus multifruits">Jus multifruits 1.00L
                                    </option>
                                    <option class="text-center" value="Schweppes">Schweppes
                                    </option>
                                    <option class="text-center" value="San Pellegrino Sanbitter">San Pellegrino Sanbitter 0.1L
                                    </option>
                                    <option class="text-center" value="Jus de pomme">Jus de pomme 0.5L
                                    </option>
                                    <option class="text-center" value="Rivella Rouge">Rivella Rouge 0.5L
                                    </option>






                                </select>


                            </div>

                            <div class="col-md-6 " style="">

                                <h3 id="prixBoissonschoice">5.00 CHF</h3>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezmenuchoiceBoissons">  Commandez </button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

            <div class="col-md-12" style="margin-top: 1cm" id="cid-Desserts">
                <h2>Desserts</h2>
                <hr>


                <div class="card">
                    <img src="assets/images/dessert.jpg" alt="Avatar" style="width:100%" >
                    <div class="container">
                        <h4>Menu Desserts</h4>
                        <p>Obtenir votre Desserts</p>

                    </div>
                    <div class="container"  id="menuchoiceDesserts">
                        <div class="row"   style="margin-top: 1cm;" >
                            <div class="col-md-6"><p>Desserts </p></div>
                            <div class="col-md-6 " style="">

                                <select class="form-control"  id="Dessertschoices"  onchange="loadDesserts()">
                                    <option class="text-center" value="Tiramisu Maison">Tiramisu Maison
                                    </option>
                                    <option class="text-center" value="Baklava (3 pièces )">Baklava (3 pièces )
                                    </option>
                                    <option class="text-center" value="Coupe Danemark (190ml)">Coupe Danemark (190ml)
                                    </option>
                                    <option class="text-center" value="Coupe Café Glacé (190ml)">Coupe Café Glacé (190ml)
                                    </option>
                                    <option class="text-center" value="Glacé MövenpickVanille">Glacé Mövenpick Vanille 900 ML</option>
                                    <option class="text-center" value="Glacé MövenpickChocolat">Glacé Mövenpick Chocolat 900ML</option>
                                    <option class="text-center" value="Glacé MövenpickFraise">Glacé Mövenpick Fraise 900ML</option>
                                    <option class="text-center" value="Glacé MövenpickStracciatella">Glacé Mövenpick Stracciatella 900ML</option>
                                    <option class="text-center" value="Glacé MövenpickCaramel">Glacé Mövenpick Caramel 900ML</option>
                                    <option class="text-center" value="Glacé MövenpickPistache">Glacé Mövenpick Pistache 900ML</option>
                                    <option class="text-center" value="Ben & Jerry's Cookie Dough">Ben & Jerry's Cookie Dough 900ML
                                    </option>
                                    <option class="text-center" value="GlaceCaramel">Glace Caramel</option>
                                    <option class="text-center" value="GlaceStracciatella">Glace Stracciatella</option>
                                    <option class="text-center" value="GlaceVanille">Glace Vanille</option>
                                    <option class="text-center" value="Tourte Framboise (75g)">Tourte Framboise (75g)
                                    </option>




                                </select>


                            </div>

                            <div class="col-md-6 " style="">

                                <h3 id="prixDessertschoice">6.00 CHF</h3>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezmenuchoiceDesserts">  Commandez </button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>


            <div class="col-md-12" style="margin-top: 1cm" id="cid-Bieres">
                <h2>Bières</h2>
                <hr>
                <div class="card">
                    <img src="assets/images/biere2.jpg" alt="Avatar" style="width:100%" >
                    <div class="container">
                        <h4>Menu Bieres</h4>
                        <p>Obtenir votre Biere</p>

                    </div>
                    <div class="container"  id="menuchoiceBiere">
                        <div class="row"   style="margin-top: 1cm;" >
                            <div class="col-md-6"><p>Bieres </p></div>
                            <div class="col-md-6 " style="">

                                <select class="form-control"  id="Bierechoices"  >
                                    <option class="text-center" value="Feldschlösschen">Feldschlösschen 0.5L
                                    </option>
                                    <option class="text-center" value="Efes">Efes 0.5L</option>
                                    <option class="text-center" value="Heineken0.25">Heineken 0.25L</option>
                                    <option class="text-center" value="Heineken0.5">Heineken 0.5L</option>
                                    <option class="text-center" value="Desperados">Desperados 0.33 L</option>
                                    <option class="text-center" value="Corona">Corona 0.33L</option>
                                    <option class="text-center" value="Sagres">Sagres 0.5L</option>
                                    <option class="text-center" value="SuperBook">SuperBook 0.25L</option>
                                    <option class="text-center" value="Six Pack Feldschlösschen">Six Pack Feldschlösschen 0.5L
                                    </option>
                                    <option class="text-center" value="Six Pack Efes">Six Pack Efes 0.25L
                                    </option>
                                    <option class="text-center" value="Six Pack Heineken">Six Pack Heineken 0.5L
                                    </option>
                                    <option class="text-center" value="Six Pack Sagres">Six Pack Sagres 0.25L
                                    </option>
                                    <option class="text-center" value="Six Pack SuperBook">Six Pack SuperBook 0.25L
                                    </option>




                                </select>


                            </div>

                            <div class="col-md-6 " style="">

                                <h3 id="prixBierechoice">5.00 CHF</h3>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-success" id="commandezmenuchoiceBiere">  Commandez </button>
                            </div>
                        </div>

                    </div>


                </div>
            </div>


                   <div class="col-md-12" style="margin-top: 1cm" id="cid-Vins">
                   <h2>Vins</h2>
                    <hr>
                    <div class="card">
                        <img src="assets/images/vin2.jpg" alt="Avatar" style="width:100%" >
                        <div class="container">
                            <h4>Menu Vins</h4>
                            <p>Obtenir votre Vins</p>

                        </div>
                        <div class="container"  id="menuchoiceVins">
                            <div class="row"   style="margin-top: 1cm;" >
                                <div class="col-md-6"><p>Vins </p></div>
                                <div class="col-md-6 " style="">

                                    <select class="form-control"  id="Vinschoices"  onchange="loadVins()">
                                        <option class="text-center" value=" N1 Assemblage Curée E.Obrist ">  N1 Assemblage Curée E.Obrist 0.375L</option>
                                        <option class="text-center" value=" N1 Curée E.Obrist "> N1 Curée E.Obrist 0.375L</option>
                                        <option class="text-center" value=" Château du manoir grand cru oeil-de-perdix AOC ">Château du manoir grand cru oeil-de-perdix AOC 0.75L</option>
                                        <option class="text-center" value=" Vevey Vin du Vigneron lavaux AOC "> Vevey Vin du Vigneron lavaux AOC 0.75L</option>
                                        <option class="text-center" value=" Vin rouge "> Vin rouge 0.75L</option>
                                        <option class="text-center" value=" Vin blanc ">Vin blanc 0.75L</option>
                                        <option class="text-center" value=" Vin Rosé "> Vin Rosé 0.75L</option>
                                        <option class="text-center" value=" Vin rouge Suisse "> Vin rouge Suisse 0.75L</option>
                                        <option class="text-center" value=" Vin rouge Étranger "> Vin rouge Étranger 0.75L</option>


                                    </select>


                                </div>

                                <div class="col-md-6 " style="">
                                    <p id="VinsDescription"> Vin rouge </p>

                                    <h3 id="prixVinschoice">19.50 CHF</h3>
                                </div>



                            </div>

                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <button class="btn btn-success" id="commandezmenuchoiceVins">  Commandez </button>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>












                </div>
            <div class="col-md-4">
                <div class="card" style="background-image: url(assets/images/CB.jpg);background-position: center;">

                    <div class="container" align="center">
                        <h4><b>Votre commande</b></h4>
                        <div class="circle">
                            <div style="margin-right:10px;margin-top:2px;color:#fff;" id="num">0</div>
                        </div>
                        <img src="assets/images/pizza_courier-2-512.png" width="50px">
                        <div id="commmd" class="row">

                        </div><br>
                        <a href="SendCommande.php" class="btn btn-success">Envoyer votre commande </a>
                        <div id="garniturep"> </div>

                    </div>


                </div>
            </div>
            </div>




    </div>



</div>

<footer class="footer-base">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 footer-center">
                    <div class="btn-group social-group btn-group-icons">
                    <a target="_blank" href="https://www.facebook.com/RoyalTacosVevey/"><i class="fa fa-facebook"></i></a>
                    <a target="_blank" href="https://www.instagram.com/royalvevey/"><i class="fa fa-instagram"></i></a>
                    </div>
                    <br>
                    Copyright 2018. Tout droits réservés ©SimpleWay.
                </div>
                
            </div>
        </div>

    </div>
    <link rel="stylesheet" href="http://templates.framework-y.com/gourmet/scripts/iconsmind/line-icons.min.css">
    <script src='assets/js/jquery.flipster.min.js'></script>
    <script async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/imagesloaded.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/parallax.min.js"></script>
    <script type="text/javascript" src='http://templates.framework-y.com/gourmet/scripts/flexslider/jquery.flexslider-min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/isotope.min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/php/contact-form.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.progress-counter.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.tab-accordion.js'></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.popover.min.js"></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/jquery.magnific-popup.min.js"></script>
    <link rel="stylesheet" href="assets/css/line-icons.min.css">
    <script src="assets/js/menucommandepizzamargerite.js"></script>
    <script src="assets/js/menuburgerschoice.js"></script>
    <script src="assets/js/Spiritueux.js"></script>
    <script src="assets/js/Vins.js"></script>
    <script src="assets/js/Biere.js"></script>
    <script src="assets/js/Boissons.js"></script>
    <script src="assets/js/Desserts.js"></script>
    <script src="assets/js/MenuPizzaChoice.js"></script>
    <script src="assets/js/menupetitfaim.js"></script>
    <script src="assets/js/menuassiette.js"></script>
    <script src="assets/js/menusalades.js"></script>
    <script src="assets/js/menupide.js"></script>
    <script src="assets/js/annimation.js"></script>
    <script src="assets/js/smoothscroll.js"></script>
    <script src="assets/js/menusandwichkabeb.js"></script>
    <script src="assets/js/menubox.js"></script>
    <script src="assets/js/menutacos.js"></script>
    <script src="assets/js/menupanini.js"></script>
    <script src="assets/js/reducer2.js"></script>
    <script src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://templates.framework-y.com/gourmet/scripts/php/contact-form.js"></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/php/datepicker.min.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/jquery.tab-accordion.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/parallax.min.js'></script>
</footer>
</body>

<!-- Mirrored from templates.framework-y.com/gourmet/pages/features/components/php-contact-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 Jan 2018 01:57:54 GMT -->
</html>

