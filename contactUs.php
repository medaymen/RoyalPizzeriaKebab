<!DOCTYPE html>
<!--[if lt IE 10]> <html  lang="en" class="iex"> <![endif]-->
<!--[if (gt IE 10)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from templates.framework-y.com/gourmet/pages/features/components/php-contact-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 Jan 2018 01:57:54 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nous contacter | Royal | Pizzeria | Kebab </title>
    <meta name="description" content="Multipurpose HTML template.">
    <script src="http://templates.framework-y.com/gourmet/scripts/jquery.min.js"></script>
    <link rel="stylesheet" href="http://templates.framework-y.com/gourmet/scripts/bootstrap/css/bootstrap.css">
    <script src="assets/js/script.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/content-box.css">
    <link rel="stylesheet" href="assets/css/image-box.css">
    <link rel="stylesheet" href="assets/css/animations.css">
    <link rel="stylesheet" href='assets/css/components.css'>
    <link rel="stylesheet" href='assets/css/flexslider.css'>
    <link rel="stylesheet" href='assets/css/magnific-popup.css'>
    <link rel="stylesheet" href='assets/css/contact-form.css'>
    <link rel="stylesheet" href='assets/css/social.stream.css'>
    <link rel="icon" href='assets/images/pi.ico'>
    <link rel="stylesheet" href="assets/css/skin.css">
    <!-- Extra optional content header -->
</head>
<body>
<div id="preloader"></div>
<header class="fixed-top bg-transparent menu-transparent scroll-change wide-area" data-menu-anima="fade-in">
    <div class="navbar navbar-default mega-menu-fullwidth navbar-fixed-top" role="navigation">
        <div class="navbar navbar-main">
            <div class="container">
            <a class="navbar-brand" href="index.php">
            <img class="logo-default scroll-hide" src="assets/images/RFB2.png" alt="logo" />
            <img class="logo-default scroll-show" src="assets/images/RFN2.png" alt="logo" />
            <img class="logo-retina" src="http://templates.framework-y.com/gourmet/images/logo-retina.png" alt="logo" />
        </a>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown active">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Acceuil <span class="caret"></span></a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="Commande.php">Commandez <span class="caret"></span></a>

                        </li>
                        <li class="dropdown">
                            <a href="contactUs.php" class="dropdown-toggle" data-toggle="dropdown" role="button">Nous Contacter <span class="caret"></span></a>

                        </li>


                    </ul>

                </div>
            </div>
        </div>
    </div>
</header>
<div class="header-title white ken-burn" data-parallax="scroll" data-position="top" style="opacity: 0.6;"    data-natural-height="850" data-natural-width="1920" data-image-src="assets/images/pizzabackground1.jpg">
    <div class="container">
        <div class="title-base">
            <h1>Nous Contacter</h1>

        </div>
    </div>
</div>
<div class="text-center">
<div class="section-bg-color">
            <div class="container content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="icon-box">
                            <div class="icon-box-cell">
                                <i class="im-phone text-xl"></i>
                            </div>
                            <div class="icon-box-cell">
                                <label class="">Nous appeler</label>
                                <p class="text-s">021.921.42.43</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-box">
                            <div class="icon-box-cell">
                            <img src='assets/images/whatsapp-icon.png'/>
                            </div>
                            <div class="icon-box-cell">
                                <label class="">Vos messages sur whatsapp</label>
                                <p class="text-s">079.443.87.45</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-box">
                            <div class="icon-box-cell">
                                <i class="im-map2 text-xl"></i>
                            </div>
                            <div class="icon-box-cell">
                                <label class="">Nous trouver</label>
                                <p class="text-d"> Av général Guisan38, 1800 Vevey</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

</div>
<div class="  text-center">
    <div class="container content">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <form id="contact" class="form-box form-ajax" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <p>Nom</p>
                            <input id="name" name="name" placeholder="Nom" type="text" class="form-control form-value" required>
                        </div>
                        <div class="col-md-6">
                            <p>Email</p>
                            <input id="email" name="email" placeholder="email" type="email" class="form-control form-value" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr class="space xs" />
                            <p>Num Tel</p>
                            <input id="phone" name="phone" placeholder="Num Tel" type="text" class="form-control form-value">
                            <hr class="space xs" />
                            <p>Message</p>
                            <textarea id="message" name="message" class="form-control form-value" required></textarea>
                            <hr class="space s" />
                            <button class="btn-sm btn" type="submit">Envoyer</button><br><br>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-3">
            </div>
        </div>

    </div>
</div>
<footer class="footer-base" >
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 footer-center">

                    
                    <div class="btn-group social-group btn-group-icons">
                        <a target="_blank" href="https://www.facebook.com/RoyalTacosVevey/"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://www.instagram.com/royalvevey/"><i class="fa fa-instagram"></i></a>
                    </div>
                    <br>
                    Copyright 2018. Tout droits réservés ©SimpleWay.

                </div>
               
            </div>
        </div>
        
    </div>
    <script>
        $("#contact").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url : 'http://www.royalpizzeriakebab.ch/ContactMail.php',
                type : 'POST',
                data: {
                    name : $("#name").val(),
                    email : $("#email").val(),
                    subject : "Reclamation ",
                    phone: $("#phone").val(),
                    message:$("#message").val(),
                    },
                success : function(result){ // success est toujours en place, bien sûr

                    /* setTimeout(function(){
                         $("#loader").attr("src","./images/success.png");
                         $(".modal-title").html("Votre mail a été envoyé avec succés !");

                     }, 3000);*/
                  alert('votre email à été envoyé avec succée !!');

                },

                error : function(result, statut, erreur){
                    /*   setTimeout(function(){
                           $(".modal-title").html("une erreur s'ést produite veuillez réessayer !");
                           $("#loader").attr("src","./images/error.png");

                       }, 3000);*/
                }

            });


        });
    </script>
    <link rel="stylesheet" href="assets/css/line-icons.min.css">
    <script src='assets/js/jquery.flipster.min.js'></script>
    <script async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/imagesloaded.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/parallax.min.js"></script>
    <script type="text/javascript" src='http://templates.framework-y.com/gourmet/scripts/flexslider/jquery.flexslider-min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/isotope.min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/php/contact-form.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.progress-counter.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.tab-accordion.js'></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.popover.min.js"></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/jquery.magnific-popup.min.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?sensor=false'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/google.maps.min.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/social.stream.min.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/smooth.scroll.min.js'></script>
</footer>
</body>

<!-- Mirrored from templates.framework-y.com/gourmet/pages/features/components/php-contact-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 Jan 2018 01:57:54 GMT -->
</html>

