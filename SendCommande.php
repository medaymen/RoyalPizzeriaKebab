<!DOCTYPE html>
<!--[if lt IE 10]> <html  lang="en" class="iex"> <![endif]-->
<!--[if (gt IE 10)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from templates.framework-y.com/gourmet/pages/features/components/php-contact-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 Jan 2018 01:57:54 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Votre commande | Royal | Pizzeria | Kebab </title>
    <meta name="description" content="Multipurpose HTML template.">
    <script src="http://templates.framework-y.com/gourmet/scripts/jquery.min.js"></script>
    <link rel="stylesheet" href="http://templates.framework-y.com/gourmet/scripts/bootstrap/css/bootstrap.css">
    <script src="assets/js/script.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/content-box.css">
    <link rel="stylesheet" href="assets/css/image-box.css">
    <link rel="stylesheet" href="assets/css/animations.css">
    <link rel="stylesheet" href='assets/css/components.css'>
    <link rel="stylesheet" href='assets/css/flexslider.css'>
    <link rel="stylesheet" href='assets/css/magnific-popup.css'>
    <link rel="stylesheet" href='assets/css/contact-form.css'>
    <link rel="stylesheet" href='assets/css/social.stream.css'>
    <link rel="icon" href='assets/images/pi.ico'>
    <link rel="stylesheet" href="assets/css/skin.css">
    <!-- Extra optional content header -->
</head>
<body>
<div id="preloader"></div>
<header class="fixed-top bg-transparent menu-transparent scroll-change wide-area" data-menu-anima="fade-in">
    <div class="navbar navbar-default mega-menu-fullwidth navbar-fixed-top" role="navigation">
        <div class="navbar navbar-main">
            <div class="container">
                <a class="navbar-brand" href="index.php">
                    <img class="logo-default scroll-hide" src="assets/images/LB.png" alt="logo" />
                    <img class="logo-default scroll-show" src="assets/images/LN.png" alt="logo" />
                    <img class="logo-retina" src="http://templates.framework-y.com/gourmet/images/logo-retina.png" alt="logo" />
                </a>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown active">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Acceuil <span class="caret"></span></a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="Commande.php">Commandez <span class="caret"></span></a>

                        </li>
                        <li class="dropdown">
                            <a href="contactUs.php" class="dropdown-toggle" data-toggle="dropdown" role="button">Nous Contacter <span class="caret"></span></a>

                        </li>


                    </ul>

                </div>
            </div>
        </div>
    </div>
</header>
<div class="header-title white ken-burn" data-parallax="scroll" data-position="top" style="opacity: 0.6;"    data-natural-height="850" data-natural-width="1920" data-image-src="assets/images/Pizza-Delivery.jpg">
    <div class="container">
        <div class="title-base">
            <h1>Envoyer Votre Commande</h1>

        </div>
    </div>
</div>
<div class="section-empty section-item text-center">
    <div class="container content">

        <hr class="space m" />
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="background-image: url(assets/images/CB.jpg);background-position: center;">

                    <div class="container" align="left">
                        <h4><b>Votre commande</b></h4>
                        <div class="circle">
                            <div style="margin-right:10px;margin-top:2px;color:#fff;" id="num">0</div>
                        </div>
                        <h2  id="totalcommande"> </h2>
                        <div id="commmd" class="row">

                        </div><br>

                        <div id="garniturep"> </div>

                    </div>


                </div>
            </div>
            <div class="col-md-6">
                <form  class="form-box form-ajax" method="post"  id="sendcommand">
                    <div align="center"> <h2>Veuillez remplir le formulaire</h2></div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <p>Nom</p>
                            <input id="name" name="name" placeholder="Nom" type="text" class="form-control form-value" required>
                        </div>
                        <div class="col-md-6">
                            <p>Prénom</p>
                            <input id="prenom" name="prenom" placeholder="Prénom" type="text" class="form-control form-value" required>
                        </div>
                        <div class="col-md-6">
                            <p>Email</p>
                            <input id="email" name="email" placeholder="email" type="email" class="form-control form-value" required>
                        </div>
                        <div class="col-md-6">
                            <p>Civilité</p>
                            <input id="civilite" name="civilite" placeholder="civilité" type="text" class="form-control form-value" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr class="space xs" />
                            <p>Choix de Paiment</p>
                            <select class="form-control" id="choixpaiment">
                                <option value="Carte">Carte</option>
                                <option value="Cash">Cash</option>
                            </select>
                            <hr class="space xs" />
                            <p>Num Tel</p>
                            <input id="phone" name="phone" placeholder="Num Tel" type="text" class="form-control form-value">
                            <hr class="space xs" />
                            <p>Rue</p>
                            <input id="Rue" name="Rue" placeholder="Rue" type="text" class="form-control form-value">
                            <hr class="space s" />
                            <p>code immeuble</p>
                            <textarea id="codeimmeuble" name="codeimmeuble"   placeholder="Code Immeuble" class="form-control form-value" ></textarea>
                            <hr class="space s" />
                            <p>étage</p>
                            <textarea id="etage" name="etage"  placeholder="étage"  class="form-control form-value" ></textarea>
                            <hr class="space s" />
                            <p>Remarque</p>
                            <textarea id="remarque"  placeholder="Remarque" name="Remarque" class="form-control form-value" required></textarea>
                            <hr class="space s" />
                            <div id="butt">
                                <button class="btn-sm btn"   type="submit">Envoyer</button>
                            </div>

                        </div>
                    </div>


                </form>
            </div>

            </div>
        </div>

    </div>
</div>
<footer class="footer-base" >
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 footer-center">


                    <div class="btn-group social-group btn-group-icons">
                    <a target="_blank" href="https://www.facebook.com/RoyalTacosVevey/"><i class="fa fa-facebook"></i></a>
                    <a target="_blank" href="https://www.instagram.com/royalvevey/"><i class="fa fa-instagram"></i></a>
                    </div>
                    <br>
                    Copyright 2018. Tout droits réservés ©SimpleWay.

                </div>

            </div>
        </div>

    </div>
    <link rel="stylesheet" href="assets/css/line-icons.min.css">
    <script src='assets/js/jquery.flipster.min.js'></script>
    <script src="assets/js/reducer.js"></script>
    <script async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/imagesloaded.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/parallax.min.js"></script>
    <script type="text/javascript" src='http://templates.framework-y.com/gourmet/scripts/flexslider/jquery.flexslider-min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/isotope.min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/php/contact-form.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.progress-counter.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.tab-accordion.js'></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.popover.min.js"></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/jquery.magnific-popup.min.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?sensor=false'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/google.maps.min.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/social.stream.min.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/smooth.scroll.min.js'></script>
</footer>
</body>

<!-- Mirrored from templates.framework-y.com/gourmet/pages/features/components/php-contact-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 Jan 2018 01:57:54 GMT -->
</html>

