$(document).ready(function () {
 var totalsalade = 5.00;
 var garniture="";
 var sauce=  "sauce française";

    $("#Salades").change(function () {
        switch($("#Salades").val()){
            case "Salade verte" :
                garniture="pas de garnitures";
                $("#totalsalade").html("5.00 CHF");
                totalsalade = 5.00 ;
                sauce=  $("#totalsalade").val();
                $("#totalsalade").show();

                break;
            case "Salade mixte" :
                garniture="Salade, carotte et tomate";
                $("#totalsalade").html("8.00 CHF");
                totalsalade = 8.00 ;
                sauce=  $("#typesalade").val();
                $("#typesalade").show();
                break;
            case "Salade mêlée" :

                garniture="Salade, carotte, tomate, chou rouge, chou blanc et mais";
                $("#totalsalade").html("7.00 CHF");
                totalsalade = 7.00 ;
                sauce=  $("#typesalade").val();
                $("#typesalade").show();
                break;
            case "Salade grecque" :
                garniture="Salade, carotte, chou blanc, fromage feta, ilive, et concombre";
                $("#totalsalade").html("13.00 CHF");
                totalsalade = 13.00 ;
                sauce=  $("#typesalade").val();
                $("#typesalade").show();
                break;
            case "Salade César" :
                garniture="Crudité du jour, blanc de poulet, oeuf dur et croûtons de pain";
                $("#totalsalade").html("14.50 CHF");
                totalsalade = 14.50 ;
                sauce=  $("#typesalade").val();
                $("#typesalade").show();

                break;
            case "Salade au thon" :
                garniture="Crudité du jour, thon et olive";
                $("#totalsalade").html("14.50 CHF");
                totalsalade = 14.50 ;
                sauce=  $("#typesalade").val();
                $("#typesalade").show();
                break;

        }

    });





    $("#commandezsalade").click(function () {
        var menusSalade = {
            type : $("#Salades").val(),
            plus: garniture + "<br> sauce : "+  $("#typesalade").val(),
            total:  totalsalade


        };

        document.dispatchEvent(new CustomEvent('commandezsalade',{'detail': menusSalade}));



    });
});