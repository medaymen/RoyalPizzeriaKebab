var   priceboisson = 3.50;

function loadBoissons(){
    switch($("#Boissonschoices").val()) {
        case "Eau minérale gazeuse0.5" :
            $("#prixBoissonschoice").html("3.50 CHF");
            priceboisson = 3.50;
            break;
        case "Eau minérale gazeuse1.5" :
            $("#prixBoissonschoice").html("6.00 CHF");
            priceboisson = 6.00;
            break;

        case "Eau minérale naturelle0.5" :
            $("#prixBoissonschoice").html("3.50 CHF");
            priceboisson = 3.50;
            break;

        case "Eau minérale naturelle1.5" :
            $("#prixBoissonschoice").html("6.00 CHF");
            priceboisson = 6.00;
            break;

        case "Coca Cola0.33" :
            $("#prixBoissonschoice").html("3.00 CHF");
            priceboisson = 3.00;
            break;

        case "Coca Cola0.5" :
            $("#prixBoissonschoice").html("4.00 CHF");
            priceboisson = 4.00;
            break;

        case "Coca Cola1.5" :
            $("#prixBoissonschoice").html("7.00 CHF");
            priceboisson = 7.00;
            break;

        case "Coca Cola Zero0.5" :
            $("#prixBoissonschoice").html("3.00 CHF");
            priceboisson = 3.00;
            break;

        case "Coca Cola Zero1.5" :
            $("#prixBoissonschoice").html("7.00 CHF");
            priceboisson = 7.00;
            break;

        case "Fanta Orange0.5" :
            $("#prixBoissonschoice").html("4.00 CHF");
            priceboisson = 4.00;
            break;

        case "Fanta Orange1.5" :
            $("#prixBoissonschoice").html("7.50 CHF");
            priceboisson = 7.50;
            break;

        case "Fanta Shokota" :
            $("#prixBoissonschoice").html("4.00 CHF");
            priceboisson = 4.00;
            break;

        case "Fanta Lémon" :
            $("#prixBoissonschoice").html("3.00 CHF");
            priceboisson = 3.00;
            break;

        case "Fanta Fraise Kiwi" :
            $("#prixBoissonschoice").html("3.00 CHF");
            priceboisson = 3.00;
            break;

        case "Fanta Exotique" :
            $("#prixBoissonschoice").html("3.00 CHF");
            priceboisson = 3.00;
            break;

        case "Sprite0.5" :
            $("#prixBoissonschoice").html("4.00 CHF");
            priceboisson = 4.00;
            break;

        case "Sprite1.5" :
            $("#prixBoissonschoice").html("7.00 CHF");
            priceboisson = 7.00;
            break;

        case "Thé Froid Peach0.5" :
            $("#prixBoissonschoice").html("4.00 CHF");
            priceboisson = 4.00;
            break;

        case "Thé Froid Peach1.5" :
            $("#prixBoissonschoice").html("7.00 CHF");
            priceboisson = 7.00;
            break;

        case "Thé Froid Lemon0.5" :
            $("#prixBoissonschoice").html("4.00 CHF");
            priceboisson = 4.00;
            break;

        case "Thé Froid Lemon1.5" :
            $("#prixBoissonschoice").html("7.00 CHF");
            priceboisson = 7.00;
            break;

        case "Uludag Orange" :
            $("#prixBoissonschoice").html("4.00 CHF");
            priceboisson = 4.00;
            break;

        case "Ayran" :
            $("#prixBoissonschoice").html("4.00  CHF ");
            priceboisson = 4.00;
            break;

        case "Red Bull" :
            $("#prixBoissonschoice").html("4.50 CHF");
            priceboisson = 4.50;
            break;

        case "Arizona thé vert" :
            $("#prixBoissonschoice").html("5.50 CHF");
            priceboisson = 5.50;
            break;

        case "Oishi Thé vert Citron Miel" :
            $("#prixBoissonschoice").html("5.00 CHF");
            priceboisson = 5.00;
            break;

        case "Jus d'orange" :
            $("#prixBoissonschoice").html("7.00 CHF");
            priceboisson = 7.00;
            break;

        case "Jus multifruits" :
            $("#prixBoissonschoice").html("7.00 CHF");
            priceboisson = 7.00;
            break;

        case "Schweppes" :
            $("#prixBoissonschoice").html("4.50 CHF");
            priceboisson = 4.50;
            break;

        case "San Pellegrino Sanbitter" :
            $("#prixBoissonschoice").html("3.90 CHF");
            priceboisson = 3.90;
            break;

        case "Jus de pomme" :
            $("#prixBoissonschoice").html("4.50 CHF");
            priceboisson = 4.50;
            break;

        case "Rivella Rouge" :
            $("#prixBoissonschoice").html("4.50 CHF");
            priceboisson = 4.50;
            break;









    }
}
$("#commandezmenuchoiceBoissons").click(function () {
    var menuchoiceBoissons ={

        type : $("#Boissonschoices").val(),
        plus : "aucun",
        total : priceboisson
    };
    document.dispatchEvent(new CustomEvent('commandezmenuchoiceBoissons',{'detail': menuchoiceBoissons}));

});