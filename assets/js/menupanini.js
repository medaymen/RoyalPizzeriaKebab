
$(document).ready(function () {
    var panini="Viande hachée Taco";
    var totalpanini= 10.00;
    var garniture = [];
    $(".garniturespanini").click(function () {

        if($(this).is( ":checked", true )){
            switch ( $(this).val()){
                case "Salade":
                    garniture.push("Salade");
                    console.log(garniture);
                    break;
                case "Oignon":
                    garniture.push("Oignon");   console.log(garniture);
                    break;
                case  "Tomate":
                    garniture.push("Tomate");   console.log(garniture);
                    break;
                case  "Saucepiquant":
                    garniture.push("Sauce piquant");   console.log(garniture);
                    break;
                case  "saucecocktail":
                    garniture.push("Sauce cocktail");   console.log(garniture);
                    break;
                case  "saucealgerienne":
                    garniture.push("Sauce algerienne");   console.log(garniture);
                    break;
                case  "sauceblanche":
                    garniture.push("Sauce blanche");   console.log(garniture);
                    break;
                case  "saucetartare":
                    garniture.push("Sauce tartare");   console.log(garniture);
                    break;
                case  "saucecurry":
                    garniture.push("Sauce curry");   console.log(garniture);
                    break;
                case  "saucemayonnaise":
                    garniture.push("Sauce mayonnaise");   console.log(garniture);
                    break;
                case  "sauceketchup":
                    garniture.push("Sauce ketchup");   console.log(garniture);
                    break;

            }
        }else {
            switch ( $(this).val()){
                case "Salade":

                    var index = garniture.indexOf("Salade");
                    garniture.splice(index,1);


                    break;
                case "Oignon":

                    var index = garniture.indexOf("Oignon");
                    garniture.splice(index,1);

                    break;
                case  "Tomate":

                    var index = garniture.indexOf("Tomate");
                    garniture.splice(index,1);

                    break;
                case  "Saucepiquant":
                    var index = garniture.indexOf("Sauce piquant");
                    garniture.splice(index,1);

                    break;
                case  "saucecocktail":
                    var index = garniture.indexOf("Sauce cocktail");
                    garniture.splice(index,1);

                    break;
                case  "saucealgerienne":
                    var index = garniture.indexOf("Sauce algerienne");
                    garniture.splice(index,1);

                    break;
                case  "sauceblanche":
                    var index = garniture.indexOf("Sauce blanche");
                    garniture.splice(index,1);

                    break;
                case  "saucetartare":
                    var index = garniture.indexOf("Sauce tartare");
                    garniture.splice(index,1);

                    break;
                case  "saucecurry":
                    var index = garniture.indexOf("Sauce curry");
                    garniture.splice(index,1);

                    break;
                case  "saucemayonnaise":
                    var index = garniture.indexOf("Sauce mayonnaise");
                    garniture.splice(index,1);

                    break;
                case  "sauceketchup":
                    var index = garniture.indexOf("Sauce ketchup");
                    garniture.splice(index,1);

                    break;


            }
        }


    });
    $("#panini").change(function () {

        switch ( $(this).val()){
            case "Panini Agneau":
                totalpanini = 10.00 ;
                panini = "Panini Agneau";
                $("#totalpanini").html("Total :"+totalpanini+"  CHF");
                $("#totall2panini").html(totalpanini);



                break;
            case  "Panini Chorizo":
                totalpanini = 10.00 ;
                panini = "Panini Chorizo";
                $("#totalpanini").html("Total :"+totalpanini+"  CHF");
                $("#totall2panini").html(totalpanini);

                break;
            case  "Panini viande séche":
                totalpanini = 10.00 ;
                panini = "Panini viande séche";
                $("#totalpanini").html("Total :"+totalpanini+"  CHF");
                $("#totall2panini").html(totalpanini);


                break;
            case  "Panini jambon":
                totalpanini = 10.00 ;


                panini = "Panini jambon";
                $("#totalpanini").html("Total :"+totalpanini+"  CHF");
                $("#totall2panini").html(totalpanini);

                break;
            case "Panini Tomate Mozzarella":
                totalpanini = 10.00 ;
                panini = "Panini Tomate Mozzarella";
                $("#totalpanini").html("Total :"+totalpanini+"  CHF");
                $("#totall2panini").html(totalpanini);

                break;
            case "Panini Mixte":

                panini = "Panini Mixte (Trois viandes maximum)";
                totalpanini = 13.00 ;
                $("#totalpanini").html("Total :"+totalpanini+"  CHF");
                $("#totall2panini").html(totalpanini);


                break;



        }

    });
    $("#commandezpanini").click(function () {
        var paniniObject = null;
        if(garniture.length <=0){
            paniniObject = {
                type: panini ,
                plus: "aucun",
                total: totalpanini


            };
        }else {
            paniniObject = {
                type: panini ,
                plus: garniture.join('-'),
                total: totalpanini


            };
        }


        console.log(garniture);


        document.dispatchEvent(new CustomEvent('commandepanini',{'detail': paniniObject}));

    });
});
