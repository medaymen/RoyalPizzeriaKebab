function loadBurgers(){
    var pp= 6.00;
    switch($("#Burgerschoice").val()) {
        case "Hamburger" :
            $("#prixBurgerschoice").html("6.00");
            $("#BurgersDescription").html("Un steak, salade, tomates, oignons, ketchup et mayonnaise\n");
            pp=6.00;
            break;

        case "Big City Burger et pommes frites" :
            $("#prixBurgerschoice").html("18.00");
            $("#BurgersDescription").html("Viande (160g), chou blanc, mayonnaise, pommes frites et un coca-cola (0.33l)");
            pp=18.00;
            break;
        case "MaisonBurger" :
            $("#prixBurgerschoice").html("22.00");
            $("#BurgersDescription").html("Un steak maison, pain maison, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=22.00;
            break;
        case "Cheeseburger" :
            $("#prixBurgerschoice").html("7.00");
            $("#BurgersDescription").html("Un steak, fromage, salade, tomates, oignons, ketchup et mayonnaise\n");
            pp=7.00;
            break;

        case "DoubleBurgerOeuf" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, œuf, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "DoubleBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "DoubleCheeseburgerOeuf" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, oeuf, fromage, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "DoubleCheeseburger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, fromage, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "BIGBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "BIGCheeseburger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, fromage, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "CrèveLaDalle" :
            $("#prixBurgerschoice").html("25.00");
            $("#BurgersDescription").html("Quatres steaks, salade, tomates, oignons, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=25.00;
            break;

        case "LingLingBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, fromage Gruyère, salade, tomates et ketchup, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "ClassicBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, ketchup, mayonnaise, salade, oignons grillés et tomates, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "RonaldoBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, salade et sauce à l'ail, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "MilanoBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, tomates, mozzarella, basilic et huile d'olive, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "HamburgerGruyère" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, tomates, oignons, fromage Gruyère et une sauce aux choix, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "HamburgerRaclette" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, tomates, oignons, fromage raclette et une sauce aux choix, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "HamburgerTroisFromages" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux Steaks, fromage Gruyère, fromage raclette, tomates, oignons et une sauce aux choix, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "Route66Burger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, salade, oignons grillés, champignons grillés et une sauce aux choix (sauce recommendée: sauce Biggy-Burger), accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "BurgerTorooo" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, poivrons, oignons grillés, champignons grillés, fromage et une sauce aux choix, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "CampagnardBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, bacon, oignons grillés et une sauce aux choix (sauce recommendée: sauce barbecue), accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "BilkessBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, cheddar, salade, tomates, oignons rouges, ketchup et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)");
            pp=21.00;
            break;

        case "OassasBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, fromage Gorgonzola, roquette et oignons grillés, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "VeveysanBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, fromage Emmental, oignons grillés, bacon, salade, tomates et sauce aigre-douce, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "RivieraBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Deux steaks, fromage de chèvre, salade, tomates, oignons grillés et sauce Biggy-Burger, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "TutédrakuBurger" :
            $("#prixBurgerschoice").html("25.00");
            $("#BurgersDescription").html("Quatres steaks, salade, tomates, oignons grillés, cheddar, chorizo et spécial sauce, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=25.00;
            break;

        case "ChorizoBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, oignons, fromage, chorizo, piquant et ketchup, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "JambonBurger" :
            $("#prixBurgerschoice").html("21.00");
            $("#BurgersDescription").html("Trois steaks, oignons, fromage, jambon et mayonnaise, accompagné par une portion de frites et un Coca Cola (0.33 l)\n");
            pp=21.00;
            break;

        case "LeMagic" :
            $("#prixBurgerschoice").html("13.00");
            $("#BurgersDescription").html("Filet de poulet, fromage, salade, oignons frais, mayonnaise et une sauce aux choix (sauce recommendée: sauce barbecue)");
            pp=13.00;
            break;

        case "LePaysan" :
            $("#prixBurgerschoice").html("13.00");
            $("#BurgersDescription").html("Filet de poulet, fromage, salade, oignons, frites et une sauce aux choix (sauce recommendée: moutarde)");
            pp=13.00;
            break;
        case "ElPasha" :
            $("#prixBurgerschoice").html("13.00");
            $("#BurgersDescription").html("Filet de poulet, oignons, tomates et une sauce aux choix (sauce recommendée: sauce piquante)");
            pp=13.00;
            break;

        case "Cocorico" :
            $("#prixBurgerschoice").html("13.00");
            $("#BurgersDescription").html("Filet de poulet, tomates, salade, oignons et une sauce aux choix (sauce recommendée: sauce Algérienne)");
            pp=13.00;
            break;

        case "BurgerCordonBleu" :
            $("#prixBurgerschoice").html("13.00");
            $("#BurgersDescription").html("Cordon Bleu, ingrédients aux choix (sauce recommendée: sauce Tartare)");
            pp=13.00;
            break;

        case "FalafelBurger" :
            $("#prixBurgerschoice").html("13.00");
            $("#BurgersDescription").html("Falafel, ingrédients aux choix (sauce recommendée: sauce Blanche)");
            pp=13.00;
            break;






    }
}
$("#commandezmenuchoiceBurgers").click(function () {
    var menuchoiceBurgers ={

        type : $("#Burgerschoice").val()+"cs",
        plus : "aucun",
        total : pp
    };
    document.dispatchEvent(new CustomEvent('commandezmenuchoiceBurgers',{'detail': menuchoiceBurgers}));

});
