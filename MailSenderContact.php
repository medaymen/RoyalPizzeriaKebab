<?php

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');
require_once 'PHPMailer/PHPMailerAutoload.php';
require_once 'PHPMailer/class.phpmailer.php';
class MailSenderContact{
    private $_mailer;
    private $_result='';
    private $_state=false;
    private  $_recipient;
    private $_subject;
    private $_message;
    private $_BCC;//$this->_mailer->addBCC($this->_BCC);
    private $_gmail=true;
    private $_sendfrom;
    private $_sendmailfrom;
    private $_numtel;
    private $_ville;
    private $_adresse;
    public function __construct()
    {
        $this->_mailer = new PHPMailer;
        //$this->_mailer->IsSMTP();// Set mailer to use SMTP
        $this->_mailer->SMTPAuth = true; // Set mailer to use SMTP
        if ($this->_gmail) {
            $this->_mailer->Host = 'smtp.gmail.com';  // Gmail smtp
            $this->_mailer->Port = 465 ; // TCP port to connect to (Defaut 25)
            $this->_mailer->SMTPSecure = true;
            $this->_mailer->SMTPSecure = "tls";// Enable TLS encryption, `ssl` also accepted /secure transfer enabled REQUIRED for Gmail
        }
        $this->_mailer->SMTPDebug = 0;    // Enable SMTP authentication
        $this->_mailer->Username = 'rvevey@gmail.com';  // SMTP username
        $this->_mailer->Password = 'Tanri1993verdi';  // SMTP assword
        $this->_mailer->addReplyTo("name@example.com", "Recepient Name");
        $this->_mailer->From = 'rvevey@gmail.com';
        $this->_mailer->FromName = 'Royal rvevey contact ';

    }
    public function sendMail(){
        // Add a recipient
        $this->_mailer->addAddress($this->_recipient);
        $this->_mailer->addBCC($this->_BCC);
        $this->_mailer->isHTML(true);  // Set email format to HTML
        $this->_mailer->Subject = $this->_subject;
// HTML email body
        $body  = "<html><body>";
        $body .= "<table width='100%' bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0'>";
        $body .= "<tr><td>";
        $body .= "<table align='center' width='100%' border='0' cellpadding='0' cellspacing='0' style='max-width:650px; background-color:#fff; font-family:Verdana, Geneva, sans-serif;'>";
        $body .= "<thead>
						<tr height='80'>
							<th colspan='4' style='background-color:#f5f5f5; border-bottom:solid 1px #bdbdbd; font-family:Verdana, Geneva, sans-serif; color:#333; font-size:34px;'>Royal vevey  Réclamation</th>
						</tr>
						</thead>";
        $body .= "<tbody>
						<tr align='center' height='50' style='font-family:Verdana, Geneva, sans-serif;background-color:#00a2d1;'>
						<td colspan='4' align='center' style='background-color:#f5f5f5; border-top:dashed #00a2d1 2px; font-size:24px; '>
						</tr>
						<tr>
							<td colspan='4' style='padding:15px;'>
                                <p style='font-size:12px;'>Message de la part de  : ".$this->getSendFrom()."</p>
                                <p style='font-size:12px;'>Numéro de téléphone : ".$this->getNumTel()."</p>
                                <p style='font-size:12px;'>Email : ".$this->getSendmailfrom()."</p>
								<p style='font-size:13px; font-family:Verdana, Geneva, sans-serif; text-justify: auto'>". $this->_message.".</p>
							</td>
						</tr>
						
						</tbody>";
        $body .= "</table>";
        $body .= "</td></tr>";
        $body .= "</table>";
        $body .= "</body></html>";
        // HTML email body
        $this->_mailer->AltBody = $this->_message;
        $this->_mailer->MsgHTML($body);
        if(!$this->_mailer->send()) {
            $this->_result = '<p> Mail error:' .$this->_mailer->ErrorInfo;
            $this->_state=false;
        }else{
            $this->_result = '<p> Message sent!</p>';
            $this->_state=true;
        }
    }
    public function getresult(){
        return $this->_result;
    }
    public function setrecipient($value){
        $this->_recipient=$value;
    }
    public function setsubject($value){
        $this->_subject=$value;
    }
    public function setMessage($value){
        $this->_message=$value;
    }
    public function setBBC($value){
        $this->_BCC=$value;
    }
    public function setstate($state)
    {
        $this->_state = $state;
    }
    public function getstate()
    {
        return $this->_state;
    }
    public function setSendFrom($sendfrom)
    {
        $this->_sendfrom = $sendfrom;
    }
    public function getSendFrom()
    {
        return $this->_sendfrom;
    }

    /**
     * @return mixed
     */
    public function getNumtel()
    {
        return $this->_numtel;
    }

    /**
     * @param mixed $numtel
     */
    public function setNumtel($numtel)
    {
        $this->_numtel = $numtel;
    }

    /**
     * @return mixed
     */
    public function getSendmailfrom()
    {
        return $this->_sendmailfrom;
    }

    /**
     * @param mixed $sendmailfrom
     */
    public function setSendmailfrom($sendmailfrom)
    {
        $this->_sendmailfrom = $sendmailfrom;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->_ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville)
    {
        $this->_ville = $ville;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->_adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->_adresse = $adresse;
    }


}
