﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Accueil | Royal | Pizzeria | Kebab </title>
    <meta name="description" content="Multipurpose HTML template.">
    <script src="http://templates.framework-y.com/gourmet/scripts/jquery.min.js"></script>
    <link rel="stylesheet" href="http://templates.framework-y.com/gourmet/scripts/bootstrap/css/bootstrap.css">
    <script src="assets/js/script.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/content-box.css">
    <link rel="stylesheet" href="assets/css/image-box.css">
    <link rel="stylesheet" href="assets/css/animations.css">
    <link rel="stylesheet" href='assets/css/components.css'>
    <link rel="stylesheet" href='assets/css/flexslider.css'>
    <link rel="stylesheet" href='assets/css/magnific-popup.css'>
    <link rel="stylesheet" href='assets/css/contact-form.css'>
    <link rel="stylesheet" href='assets/css/social.stream.css'>
    <link rel="icon" href='assets/images/pi.ico'>
    <link rel="stylesheet" href="assets/css/skin.css">
    <!-- Extra optional content header -->
</head>
<body>
    <div id="preloader"></div>
    <div class="footer-parallax-container">
        <header class="fixed-top bg-transparent menu-transparent scroll-change wide-area" data-menu-anima="fade-in">
            <div class="navbar navbar-default mega-menu-fullwidth navbar-fixed-top" role="navigation">
                <div class="navbar navbar-main">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle">
                                <i class="fa fa-bars"></i>
                            </button>
                            <a class="navbar-brand" href="index.php">
                                <img class="logo-default scroll-hide" src="assets/images/RFB2.png" alt="logo" />
                                <img class="logo-default scroll-show" src="assets/images/RFN2.png" alt="logo" />
                                <img class="logo-retina" src="http://templates.framework-y.com/gourmet/images/logo-retina.png" alt="logo" />
                            </a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="dropdown active">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Acceuil <span class="caret"></span></a>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="Commande.php">Commandez <span class="caret"></span></a>

                                </li>
                                <li class="dropdown">
                                    <a href="contactUs.php" class="dropdown-toggle" data-toggle="dropdown" role="button">Nous Contacter <span class="caret"></span></a>

                                </li>

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </header>        <div class="section-slider row-21">
            <div class="flexslider visible-dir-nav advanced-slider slider white" data-options="animation:fade">
                <ul class="slides">
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/1.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="assets/images/1P.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/2.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="assets/images/2P.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/3.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="assets/images/3P.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/4.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="http://templates.framework-y.com/gourmet/images/pizza-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/5.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="assets/images/5P.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/6.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="http://templates.framework-y.com/gourmet/images/pizza-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/7.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="http://templates.framework-y.com/gourmet/images/pizza-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000" data-timeline="asc" data-timeline-time="200">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('assets/images/8.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner">
                                        <hr class="space visible-sm" />
                                        <hr class="space m" />
                                        <div class="row">

                                            <div class="col-md-6 anima anima-show-scale">
                                                <img src="http://templates.framework-y.com/gourmet/images/pizza-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="section-bg-image parallax-window" data-sub-height="0" data-bleed="0" data-natural-height="850" data-natural-width="1920" data-parallax="scroll" data-image-src="http://templates.framework-y.com/gourmet/images/bg-8.jpg">
            <div class="container content">
                <div class="title-base">
                    <hr />
                    <h2>Bienvenue</h2>

                </div>
            <div>
                <center> <a href="Commande.php" class="btn btn-success">Commandez </a>
                </center>
            </div>
            </div>

            </div>

        </div>

    </div>
    <i class="scroll-top scroll-top-mobile show fa fa-sort-asc"></i>

</body>
<footer class="footer-base" >
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 footer-center">

                    
                    <div class="btn-group social-group btn-group-icons">
                    <a target="_blank" href="https://www.facebook.com/RoyalTacosVevey/"><i class="fa fa-facebook"></i></a>
                    <a target="_blank" href="https://www.instagram.com/royalvevey/"><i class="fa fa-instagram"></i></a>
                    </div><br>
                    Copyright 2018. Tout droits réservés ©SimpleWay.
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        
    </div>
    <link rel="stylesheet" href="assets/css/line-icons.min.css">
    <script src='assets/js/jquery.flipster.min.js'></script>
    <script async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/imagesloaded.min.js"></script>
    <script type="text/javascript" src="http://templates.framework-y.com/gourmet/scripts/parallax.min.js"></script>
    <script type="text/javascript" src='http://templates.framework-y.com/gourmet/scripts/flexslider/jquery.flexslider-min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/isotope.min.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/php/contact-form.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.progress-counter.js'></script>
    <script type="text/javascript" async src='http://templates.framework-y.com/gourmet/scripts/jquery.tab-accordion.js'></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/bootstrap/js/bootstrap.popover.min.js"></script>
    <script type="text/javascript" async src="http://templates.framework-y.com/gourmet/scripts/jquery.magnific-popup.min.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?sensor=false'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/google.maps.min.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/social.stream.min.js'></script>
    <script src='http://templates.framework-y.com/gourmet/scripts/smooth.scroll.min.js'></script>
</footer>
</html>
